unit uConnection;

interface

uses
  mORMot,
  SynDBUniDAC,
  SynCommons,
  mORMotDB,
  mORMotSQLite3,
  SynDB,
  System.Classes,
  System.SysUtils,
  Vcl.Dialogs,
  SynSQLite3Static,
  SynDBSQLite3;

type
  TMConnection = class(TComponent)
  private
    FDriver: string;
    FServer: string;
    FPort: string;
    FUserName: string;
    FPassword: string;
    FDatabase: string;
    FProp: TSQLDBConnectionProperties;
    ConnectionString: string;
    FActive: Boolean;
    FisServerDb: Boolean;
    procedure SetActive(const Value: Boolean);
  public
    property Driver: string read FDriver write FDriver;
    property Server: string read FServer write FServer;
    property Port: string read FPort write FPort;
    property UserName: string read FUserName write FUserName;
    property Password: string read FPassword write FPassword;
    property Database: string read FDatabase write FDatabase;
    property Active: Boolean read FActive write SetActive;
    constructor Create(AOwner: TComponent; FDriver: string; FServer: string;
      FPort: string; FUserName: string; FPassword: string; FDatabase: string); overload;
    constructor Create(AOwner: TComponent; FDatabase: string); overload;
    destructor Destroy; override;
    procedure execute(sql: string);
    function getConnectionProp: TSQLDBConnectionProperties;
    procedure CreateDb(Name: string);
    procedure open;
    procedure close;

  end;

implementation

{ TMConnection }

constructor TMConnection.Create(AOwner: TComponent; FDriver: string; FServer:
  string; FPort: string; FUserName: string; FPassword: string; FDatabase: string);
begin
  inherited Create(AOwner);
  Self.FDriver := FDriver;
  Self.FServer := FServer;
  Self.FPort := FPort;
  Self.FUserName := FUserName;
  Self.FPassword := FPassword;
  Self.FDatabase := FDatabase;
  FormatString('%?Server=%;Port=%', [FDriver, FServer, FPort], ConnectionString);
  FisServerDb := True;

end;

procedure TMConnection.close;
begin
Active := false;
end;

constructor TMConnection.Create(AOwner: TComponent; FDatabase: string);
begin
  inherited Create(AOwner);
  Self.FDatabase := FDatabase;
  FisServerDb := False;
end;

procedure TMConnection.CreateDb(name: string);
begin
  if Self.FisServerDb then
    Self.execute('CREATE DATABASE IF NOT EXISTS ' + name)
  else
    raise Exception.Create('this work only o a server db');
end;

destructor TMConnection.Destroy;
begin
  FProp.Free;
  inherited;
end;

procedure TMConnection.execute(sql: string);
begin
  if Active then
  begin
    FProp.ExecuteNoResult(sql, []);
  end
  else
    raise Exception.Create('CANT PERFORM THIS OPERATION ON CLOSED DATASET');

end;

function TMConnection.getConnectionProp: TSQLDBConnectionProperties;
begin
  Result := FProp;
end;

procedure TMConnection.open;
begin
  Active := true;
end;

procedure TMConnection.SetActive(const Value: Boolean);
begin
  if Self.Active = Value then
    Exit;
  FActive := Value;
  if Value = True then
  begin
    try
      FProp.Free;
      if FisServerDb then
      begin
        FProp := TSQLDBUniDACConnectionProperties.Create(ConnectionString,
          Database, UserName, Password);
      end
      else
      begin
        FProp := TSQLDBSQLite3ConnectionProperties.Create(FDatabase, '', '', '')
      end;
    except
      on e: Exception do
      begin
        FProp.Free;
        MessageDlg(e.ToString, mtError, [mbOK], 0);
      end;

    end;
  end
  else
  begin
    FProp.Free;
  end;

end;

end.

