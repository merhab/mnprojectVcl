unit uServerDbController;

interface

uses
  System.Classes,
  uConnection,
  udatabase,
  uDbManager,
  uServerParamsDbController;

type
  TDbController = class(TComponent)
  private
    con: TMConnection;
  public
    constructor Create(AOwner: TComponent; dbName: string);
    destructor destroy; override;
    procedure Connect;
    function createDbManager(aOwner: TComponent = nil): TMDBManager;
    function login(UserName,password:string):TMUser;

  end;

implementation

{ TDbController }

procedure TDbController.Connect;
begin
  con.open;
end;

constructor TDbController.Create(AOwner: TComponent; dbName: string);
begin
  inherited Create(AOwner);
  con := TMConnection.Create(Self, DB_DRIVER, DB_SERVER, SERVER_PORT, USER_NAME,
    PASS_WORD, dbName);
end;

function TDbController.createDbManager(aOwner: TComponent): TMDBManager;
begin
  Result := TMDBManager.Create(aOwner, con, CLASSES_SERVER);
end;

destructor TDbController.destroy;
begin
 //*
  inherited;
end;

function TDbController.login(UserName, password: string): TMUser;
begin
    var Man:= createDbManager(nil);
    var usr:= TMUser.CreateAndFillPrepare(Man.fClient,'loginName=? and loginPass=?',[UserName,password]);
    if usr.GetID>0 then
    begin
      var grp := TMGroup.Create(Man.fClient,Usr.FloginGroup);
    end;
end;

end.

