unit uglobalLogin;

interface

uses
  utables;

var
  db: TMDatabase;
  user: TMUser;

implementation

initialization
  db := nil;
  user := nil;


finalization
  if user <> nil then
    user.Group.Free;
  user.Free;
  db.Free;

end.

