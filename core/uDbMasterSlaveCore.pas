unit uDbMasterSlaveCore;

interface

uses
  System.Classes,
  db,
  Vcl.StdCtrls,
  Vcl.Controls,
  uInterfaces,
  SysUtils,
  Vcl.Dialogs;

type
  TLockNotifyEvent = procedure(Sender: TObject; lockVal: Boolean; var
    canChangeLock: Boolean) of object;

  TBeforeActionNotifyEvent = procedure(Sender: TObject; var canDoAction: Boolean)
    of object;

  TFilterNotifyEvent = procedure(DataSet: TDataSet; var Accept: Boolean) of object;

  TMAsterSlaveManager = class(TComponent)
  private
    FonLock: TLockNotifyEvent;
    Freadonly: Boolean;
    FonBeforeMove: TBeforeActionNotifyEvent;
    FonAfterMove: TNotifyEvent;
    FonBeforeAppend: TBeforeActionNotifyEvent;
    FonAfterAppend: TNotifyEvent;
    FonBeforePost: TBeforeActionNotifyEvent;
    FonAfterPost: TNotifyEvent;
    FonBeforeCancel: TBeforeActionNotifyEvent;
    FonAfterCancel: TNotifyEvent;
    FonBeforeDelete: TBeforeActionNotifyEvent;
    FonAfterDelete: TNotifyEvent;
    FonBeforeFilterRecord: TFilterNotifyEvent;
    FonAfterFilterREcord: TNotifyEvent;
    FonFilterRecord: TFilterNotifyEvent;
    function getActiveDatasource: TDataSource;
    procedure FilterRecordEvent(DataSet: TDataSet; var Accept: Boolean);
    procedure setOnStateChange(const Value: TNotifyEvent);
    procedure setReadonly(const Value: Boolean);
  protected
    searchWordList: TStringList;
    searchFieladName: string;
    procedure onEnter(sender: TObject);
  public
    DataSourceMaster, DataSourceSlave, FActiveDatasource: TDataSource;
    grpBoxMaster, GrpBoxSlave: TGroupBox;
    property ActiveDatasource: TDataSource read getActiveDatasource;
    property OnStateChange: TNotifyEvent write setOnStateChange;
    property onLock: TLockNotifyEvent read FonLock write FonLock;
    property onBeforeMove: TBeforeActionNotifyEvent read FonBeforeMove write
      FonBeforeMove;
    property onAfterMove: TNotifyEvent read FonAfterMove write FonAfterMove;
    property onBeforeAppend: TBeforeActionNotifyEvent read FonBeforeAppend write
      FonBeforeAppend;
    property onAfterAppend: TNotifyEvent read FonAfterAppend write FonAfterAppend;
    property onBeforePost: TBeforeActionNotifyEvent read FonBeforePost write
      FonBeforePost;
    property onAfterPost: TNotifyEvent read FonAfterPost write FonAfterPost;
    property onBeforeCancel: TBeforeActionNotifyEvent read FonBeforeCancel write
      FonBeforeCancel;
    property onBeforeDelete: TBeforeActionNotifyEvent read FonBeforeDelete write
      FonBeforeDelete;
    property onAfterDelete: TNotifyEvent read FonAfterDelete write FonAfterDelete;

    property onAfterCancel: TNotifyEvent read FonAfterCancel write FonAfterCancel;
    property onBeforeFilterRecord: TFilterNotifyEvent read FonBeforeFilterRecord
      write FonBeforeFilterRecord;
    property onAfterFilterREcord: TNotifyEvent read FonAfterFilterREcord write
      FonAfterFilterREcord;
    property onFilterRecord: TFilterNotifyEvent read FonFilterRecord write
      FonFilterRecord;
    property readonly: Boolean read Freadonly write setReadonly default False;
    constructor Create(DataSourceMaster, DataSourceSlave: TDataSource;
      grpBoxMaster, GrpBoxSlave: TGroupBox; AOwner: TComponent = nil);
    procedure lock(Val: Boolean);
    procedure save;
    procedure cancel;
    procedure Append;
    procedure Delete;
    procedure first;
    procedure next;
    procedure prior;
    procedure last;
    procedure search(text, fieldDisplayName: WideString; anyThing: Boolean = True);
  end;

  IMasterSlave = interface
    ['{558E9524-80DA-4117-85D0-789DF117C6D3}']
    function getManeger: TMAsterSlaveManager;
  end;

implementation

{ TMAsterSlaveManager }

procedure TMAsterSlaveManager.Append;
begin
  var can := True;
  if Assigned(FonBeforeAppend) then
    FonBeforeAppend(Self, can);
  if can then
  begin
    ActiveDatasource.dataSet.Append;
    if Assigned(FonAfterAppend) then
      FonAfterAppend(Self);
  end;
end;

procedure TMAsterSlaveManager.cancel;
begin
  var can := True;
  if Assigned(FonBeforeCancel) then
    FonBeforeCancel(Self, can);
  if can then
  begin
    if (ActiveDatasource.dataSet.state = dsEdit) or (ActiveDatasource.dataSet.state
      = dsInsert) then
      ActiveDatasource.dataSet.Cancel;
    if Assigned(onAfterCancel) then
      onAfterCancel(Self);
  end;
end;

constructor TMAsterSlaveManager.Create(DataSourceMaster, DataSourceSlave:
  TDataSource; grpBoxMaster, GrpBoxSlave: TGroupBox; AOwner: TComponent);
begin
  inherited Create(AOwner);
  Self.DataSourceMaster := DataSourceMaster;
  Self.DataSourceSlave := DataSourceSlave;
  Self.grpBoxMaster := grpBoxMaster;
  Self.GrpBoxSlave := GrpBoxSlave;
  Self.grpBoxMaster.OnEnter := onEnter;
  if GrpBoxSlave.ComponentCount > 0 then
    Self.GrpBoxSlave.OnEnter := onEnter;
end;

procedure TMAsterSlaveManager.Delete;
begin
  var can := True;
  if Assigned(FonBeforeDelete) then
    FonBeforeDelete(Self, can);
  if can then
  begin
    ActiveDatasource.dataSet.Delete;
    if Assigned(FonAfterDelete) then
      FonAfterDelete(Self)
  end;
end;

procedure TMAsterSlaveManager.first;
begin
  var can := True;
  if Assigned(FonBeforeMove) then
    FonBeforeMove(Self, can);
  if can then
  begin
  //TODO LOCALIZATION
    if MessageDlg('SURE ABOUT DELETING THIS RECORD?', mtConfirmation, mbOKCancel,
      0) = mrOk then
    begin

      ActiveDatasource.dataSet.First;
      if Assigned(FonAfterMove) then
        FonAfterMove(Self);
    end;
  end;

end;

function TMAsterSlaveManager.getActiveDatasource: TDataSource;
begin
  if Assigned(FActiveDatasource) then
    Result := FActiveDatasource
  else
    Result := DataSourceMaster;
end;

procedure TMAsterSlaveManager.last;
begin
  var can := True;
  if Assigned(FonBeforeMove) then
    FonBeforeMove(Self, can);
  if can then
  begin
    ActiveDatasource.dataSet.Last;
    if Assigned(FonAfterMove) then
      FonAfterMove(Self);
  end;

end;

procedure lockKids(control: TWinControl; lockVal: Boolean);
begin
  var i: Integer;
  for i := 0 to control.ControlCount - 1 do
  begin
    if Supports(control.Controls[i], ILockAble) then
    begin
      (control.Controls[i] as ILockAble).setReadonly(lockVal);
    end;
    if control.Controls[i] is TWinControl then
      lockKids(TWinControl(control.Controls[i]), lockVal);
  end;
end;

procedure TMAsterSlaveManager.lock(val: Boolean);
begin
  var canChangeLock: Boolean := True;
  if Assigned(FonLock) then
    FonLock(Self, val, canChangeLock);
  if canChangeLock then
  begin
    lockKids(grpBoxMaster.Parent, val);
    Freadonly := val;
  end;

end;

procedure TMAsterSlaveManager.next;
begin
  var can := True;
  if Assigned(FonBeforeMove) then
    FonBeforeMove(Self, can);
  if can then
  begin
    ActiveDatasource.dataSet.Next;
    if Assigned(FonAfterMove) then
      FonAfterMove(Self);
  end;

end;

procedure TMAsterSlaveManager.onEnter(sender: TObject);
begin
  if (sender = GrpBoxSlave) and (DataSourceSlave.DataSet <> nil) then
    FActiveDatasource := DataSourceSlave
  else
    FActiveDatasource := DataSourceMaster;

end;

procedure TMAsterSlaveManager.prior;
begin
  var can := True;
  if Assigned(FonBeforeMove) then
    FonBeforeMove(Self, can);
  if can then
  begin
    ActiveDatasource.dataSet.Prior;
    if Assigned(FonAfterMove) then
      FonAfterMove(Self);
  end;

end;

procedure TMAsterSlaveManager.save;
begin
  var can := True;
  if Assigned(FonBeforePost) then
    FonBeforePost(Self, can);
  if can then
  begin
    if (DataSourceMaster.dataSet.state = dsEdit) or (DataSourceMaster.dataSet.state
      = dsInsert) then
      DataSourceMaster.dataSet.Post;
    if Assigned(DataSourceSlave.DataSet) then
      if (DataSourceSlave.dataSet.state = dsEdit) or (DataSourceSlave.dataSet.state
        = dsInsert) then
        DataSourceSlave.dataSet.Post;
    if Assigned(FonAfterPost) then
      FonAfterPost(Self);
  end;
end;

procedure TMAsterSlaveManager.FilterRecordEvent(DataSet: TDataSet; var Accept: Boolean);
begin

  Accept := True;
  if Assigned(FonBeforeFilterRecord) then
    FonBeforeFilterRecord(DataSet, Accept);

  if Assigned(FonFilterRecord) then
  begin
    FonFilterRecord(DataSet, Accept);
  end
  else
  begin
    var i: Integer;
    for i := 0 to searchWordList.Count - 1 do
    begin
      Accept := Accept and DataSet.FieldByName(searchFieladName).AsString.ToUpper.Contains
        (searchWordList.Strings[i].ToUpper);
      if not Accept then
        Break;
    end;
  end;
  if Assigned(FonAfterFilterREcord) then
    FonAfterFilterREcord(DataSet);
end;




//      else
//      begin
//        var j: Integer;
//        var fldName: string;
//        for j := 0 to DataSet.FieldCount - 1 do
//          if DataSet.Fields[j].DisplayName = field then
//          begin
//            fldName := DataSet.Fields[j].FieldName;
//            Break;
//          end;
//        Accept := Accept and DataSet.FieldByName(fldName).AsString.ToUpper.Contains
//          (string(text).ToUpper);
//      end;

//    end;
procedure TMAsterSlaveManager.search(text, fieldDisplayName: WideString;
  anyThing: Boolean);
begin
  var fld: Tfield;
  for fld in activeDatasource.dataset.fields do
  begin
    if fld.displayName = fieldDisplayName then
    begin
      break;
    end;
  end;
  ActiveDatasource.dataSet.Filtered := False;
  searchWordList := TStringList.create;
  searchWordList.Delimiter := '*';
  searchWordList.DelimitedText := text;
  searchFieladName := fld.FieldName;
  if Assigned(FonFilterRecord) then
    ActiveDatasource.DataSet.OnFilterRecord := FonFilterRecord
  else
    ActiveDatasource.dataSet.OnFilterRecord := FilterRecordEvent;
  ActiveDatasource.dataSet.filtered := True;
  searchWordList.Free;
end;

procedure TMAsterSlaveManager.setOnStateChange(const Value: TNotifyEvent);
begin
  Self.DataSourceMaster.OnStateChange := Value;
  Self.DataSourceSlave.OnStateChange := Value;
end;

procedure TMAsterSlaveManager.setReadonly(const Value: Boolean);
begin
  lock(Value);
end;

end.

