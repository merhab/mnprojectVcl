unit uInterfaces;

interface

uses
  DB;

type
  ILockAble = interface
    ['{EA451299-861B-4202-8627-B19EAF7A1E49}']
    procedure setReadOnly(Val: Boolean);
    function getReadOnly: Boolean;
  end;

  IEditable = interface(ILockAble)
    ['{E4B20EDC-F8B9-41B3-BE7A-2310FF229D52}']
    procedure setText(const Value: WideString);
    function getText: WideString;
    function getFieldName: string;
    function getIsFocused: Boolean;
    procedure setFocused;
    function GetField: TField;
    function getDataSource: TDataSource;
  end;



implementation

end.

