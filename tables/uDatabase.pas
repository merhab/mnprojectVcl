unit uDatabase;

interface

uses
  mORMot,
  System.Generics.Collections,
  System.SysUtils;

type
  TMPerson = class(TSQLRecord)
  private
    Fname: WideString;
  published
    property name: WideString read Fname write Fname;

  end;

  TMGroup = class(TSQLRecord)
  private
    Fname: WideString;
  published
    property name: WideString read Fname write Fname;

  end;

  TMUser = class(TMPerson)
  private
    FloginName: WideString;
    FloginPass: WideString;
  published
    FloginGroup: TMGroup;
    property loginName: WideString read FloginName write FloginName;
    property loginPass: WideString read FloginPass write FloginPass;
    property loginGroup: TMGroup read FloginGroup write FloginGroup;
  end;

  TMDatabases = class(TSQLRecord)
  private
    Fname: string;
    Ftitle: WideString;
    FisDefault: Boolean;
    function getName: string;
  published
    property title: WideString read Ftitle write Ftitle;
    property isDefault: Boolean read FisDefault write FisDefault;
  public
    property name: string read getName;


  end;
 const CLASSES_SERVER:array of TSQLRecordClass =[TMUser,TMGroup];
implementation

{ TMPerson }



{ TMGroup }



{ TMDatabases }



function TMDatabases.getName: string;
begin
  Result := 'db_' + inttostr(Self.IDValue);
end;



end.

