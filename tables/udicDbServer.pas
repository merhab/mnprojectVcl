unit udicDbServer;

interface

uses
  Aurelius.Dictionary.Classes, 
  Aurelius.Linq;

type
  TMAdressDictionary = class;
  TMCatDictionary = class;
  TMEmailDictionary = class;
  TMPersonDictionary = class;
  TMPhoneDictionary = class;
  TMUnitDictionary = class;
  TMUserDictionary = class;
  TMUserGroupDictionary = class;
  
  IMAdressDictionary = interface;
  
  IMCatDictionary = interface;
  
  IMEmailDictionary = interface;
  
  IMPersonDictionary = interface;
  
  IMPhoneDictionary = interface;
  
  IMUnitDictionary = interface;
  
  IMUserDictionary = interface;
  
  IMUserGroupDictionary = interface;
  
  IMAdressDictionary = interface(IAureliusEntityDictionary)
    function Title: TLinqProjection;
    function Adress: TLinqProjection;
    function Rem: TLinqProjection;
    function Id: TLinqProjection;
  end;
  
  IMCatDictionary = interface(IAureliusEntityDictionary)
    function Name: TLinqProjection;
    function Id: TLinqProjection;
    function Parent: IMCatDictionary;
  end;
  
  IMEmailDictionary = interface(IAureliusEntityDictionary)
    function Title: TLinqProjection;
    function Email: TLinqProjection;
    function Rem: TLinqProjection;
    function Id: TLinqProjection;
  end;
  
  IMPersonDictionary = interface(IAureliusEntityDictionary)
    function Name: TLinqProjection;
    function Website: TLinqProjection;
    function Rem: TLinqProjection;
    function Id: TLinqProjection;
    function AdressList: IMAdressDictionary;
    function PhoneList: IMPhoneDictionary;
    function MembersList: IMPersonDictionary;
    function EmailsList: IMEmailDictionary;
    function WorkFor: IMPersonDictionary;
  end;
  
  IMPhoneDictionary = interface(IAureliusEntityDictionary)
    function Id: TLinqProjection;
    function Title: TLinqProjection;
    function Phone: TLinqProjection;
    function Rem: TLinqProjection;
  end;
  
  IMUnitDictionary = interface(IAureliusEntityDictionary)
    function Name: TLinqProjection;
    function Id: TLinqProjection;
  end;
  
  IMUserDictionary = interface(IAureliusEntityDictionary)
    function Name: TLinqProjection;
    function Website: TLinqProjection;
    function Rem: TLinqProjection;
    function Id: TLinqProjection;
    function UserName: TLinqProjection;
    function Password: TLinqProjection;
    function AdressList: IMAdressDictionary;
    function PhoneList: IMPhoneDictionary;
    function MembersList: IMPersonDictionary;
    function EmailsList: IMEmailDictionary;
    function WorkFor: IMPersonDictionary;
    function Group: IMUserGroupDictionary;
  end;
  
  IMUserGroupDictionary = interface(IAureliusEntityDictionary)
    function Name: TLinqProjection;
    function Id: TLinqProjection;
    function CanCDatabases: TLinqProjection;
    function CanCreateDb: TLinqProjection;
    function CanChangeActiveDb: TLinqProjection;
    function CanDeleteDb: TLinqProjection;
    function CanCCat: TLinqProjection;
    function CanCUnit: TLinqProjection;
  end;
  
  TMAdressDictionary = class(TAureliusEntityDictionary, IMAdressDictionary)
  public
    function Title: TLinqProjection;
    function Adress: TLinqProjection;
    function Rem: TLinqProjection;
    function Id: TLinqProjection;
  end;
  
  TMCatDictionary = class(TAureliusEntityDictionary, IMCatDictionary)
  public
    function Name: TLinqProjection;
    function Id: TLinqProjection;
    function Parent: IMCatDictionary;
  end;
  
  TMEmailDictionary = class(TAureliusEntityDictionary, IMEmailDictionary)
  public
    function Title: TLinqProjection;
    function Email: TLinqProjection;
    function Rem: TLinqProjection;
    function Id: TLinqProjection;
  end;
  
  TMPersonDictionary = class(TAureliusEntityDictionary, IMPersonDictionary)
  public
    function Name: TLinqProjection;
    function Website: TLinqProjection;
    function Rem: TLinqProjection;
    function Id: TLinqProjection;
    function AdressList: IMAdressDictionary;
    function PhoneList: IMPhoneDictionary;
    function MembersList: IMPersonDictionary;
    function EmailsList: IMEmailDictionary;
    function WorkFor: IMPersonDictionary;
  end;
  
  TMPhoneDictionary = class(TAureliusEntityDictionary, IMPhoneDictionary)
  public
    function Id: TLinqProjection;
    function Title: TLinqProjection;
    function Phone: TLinqProjection;
    function Rem: TLinqProjection;
  end;
  
  TMUnitDictionary = class(TAureliusEntityDictionary, IMUnitDictionary)
  public
    function Name: TLinqProjection;
    function Id: TLinqProjection;
  end;
  
  TMUserDictionary = class(TAureliusEntityDictionary, IMUserDictionary)
  public
    function Name: TLinqProjection;
    function Website: TLinqProjection;
    function Rem: TLinqProjection;
    function Id: TLinqProjection;
    function UserName: TLinqProjection;
    function Password: TLinqProjection;
    function AdressList: IMAdressDictionary;
    function PhoneList: IMPhoneDictionary;
    function MembersList: IMPersonDictionary;
    function EmailsList: IMEmailDictionary;
    function WorkFor: IMPersonDictionary;
    function Group: IMUserGroupDictionary;
  end;
  
  TMUserGroupDictionary = class(TAureliusEntityDictionary, IMUserGroupDictionary)
  public
    function Name: TLinqProjection;
    function Id: TLinqProjection;
    function CanCDatabases: TLinqProjection;
    function CanCreateDb: TLinqProjection;
    function CanChangeActiveDb: TLinqProjection;
    function CanDeleteDb: TLinqProjection;
    function CanCCat: TLinqProjection;
    function CanCUnit: TLinqProjection;
  end;
  
  IDefaultDictionary = interface(IAureliusDictionary)
    function MAdress: IMAdressDictionary;
    function MCat: IMCatDictionary;
    function MEmail: IMEmailDictionary;
    function MPerson: IMPersonDictionary;
    function MPhone: IMPhoneDictionary;
    function MUnit: IMUnitDictionary;
    function MUser: IMUserDictionary;
    function MUserGroup: IMUserGroupDictionary;
  end;
  
  TDefaultDictionary = class(TAureliusDictionary, IDefaultDictionary)
  public
    function MAdress: IMAdressDictionary;
    function MCat: IMCatDictionary;
    function MEmail: IMEmailDictionary;
    function MPerson: IMPersonDictionary;
    function MPhone: IMPhoneDictionary;
    function MUnit: IMUnitDictionary;
    function MUser: IMUserDictionary;
    function MUserGroup: IMUserGroupDictionary;
  end;
  
function Dic: IDefaultDictionary;

implementation

var
  __Dic: IDefaultDictionary;

function Dic: IDefaultDictionary;
begin
  if __Dic = nil then __Dic := TDefaultDictionary.Create;
  result := __Dic;
end;

{ TMAdressDictionary }

function TMAdressDictionary.Title: TLinqProjection;
begin
  Result := Prop('Title');
end;

function TMAdressDictionary.Adress: TLinqProjection;
begin
  Result := Prop('Adress');
end;

function TMAdressDictionary.Rem: TLinqProjection;
begin
  Result := Prop('Rem');
end;

function TMAdressDictionary.Id: TLinqProjection;
begin
  Result := Prop('Id');
end;

{ TMCatDictionary }

function TMCatDictionary.Name: TLinqProjection;
begin
  Result := Prop('Name');
end;

function TMCatDictionary.Id: TLinqProjection;
begin
  Result := Prop('Id');
end;

function TMCatDictionary.Parent: IMCatDictionary;
begin
  Result := TMCatDictionary.Create(PropName('Parent'));
end;

{ TMEmailDictionary }

function TMEmailDictionary.Title: TLinqProjection;
begin
  Result := Prop('Title');
end;

function TMEmailDictionary.Email: TLinqProjection;
begin
  Result := Prop('Email');
end;

function TMEmailDictionary.Rem: TLinqProjection;
begin
  Result := Prop('Rem');
end;

function TMEmailDictionary.Id: TLinqProjection;
begin
  Result := Prop('Id');
end;

{ TMPersonDictionary }

function TMPersonDictionary.Name: TLinqProjection;
begin
  Result := Prop('Name');
end;

function TMPersonDictionary.Website: TLinqProjection;
begin
  Result := Prop('Website');
end;

function TMPersonDictionary.Rem: TLinqProjection;
begin
  Result := Prop('Rem');
end;

function TMPersonDictionary.Id: TLinqProjection;
begin
  Result := Prop('Id');
end;

function TMPersonDictionary.AdressList: IMAdressDictionary;
begin
  Result := TMAdressDictionary.Create(PropName('AdressList'));
end;

function TMPersonDictionary.PhoneList: IMPhoneDictionary;
begin
  Result := TMPhoneDictionary.Create(PropName('PhoneList'));
end;

function TMPersonDictionary.MembersList: IMPersonDictionary;
begin
  Result := TMPersonDictionary.Create(PropName('MembersList'));
end;

function TMPersonDictionary.EmailsList: IMEmailDictionary;
begin
  Result := TMEmailDictionary.Create(PropName('EmailsList'));
end;

function TMPersonDictionary.WorkFor: IMPersonDictionary;
begin
  Result := TMPersonDictionary.Create(PropName('WorkFor'));
end;

{ TMPhoneDictionary }

function TMPhoneDictionary.Id: TLinqProjection;
begin
  Result := Prop('Id');
end;

function TMPhoneDictionary.Title: TLinqProjection;
begin
  Result := Prop('Title');
end;

function TMPhoneDictionary.Phone: TLinqProjection;
begin
  Result := Prop('Phone');
end;

function TMPhoneDictionary.Rem: TLinqProjection;
begin
  Result := Prop('Rem');
end;

{ TMUnitDictionary }

function TMUnitDictionary.Name: TLinqProjection;
begin
  Result := Prop('Name');
end;

function TMUnitDictionary.Id: TLinqProjection;
begin
  Result := Prop('Id');
end;

{ TMUserDictionary }

function TMUserDictionary.Name: TLinqProjection;
begin
  Result := Prop('Name');
end;

function TMUserDictionary.Website: TLinqProjection;
begin
  Result := Prop('Website');
end;

function TMUserDictionary.Rem: TLinqProjection;
begin
  Result := Prop('Rem');
end;

function TMUserDictionary.Id: TLinqProjection;
begin
  Result := Prop('Id');
end;

function TMUserDictionary.UserName: TLinqProjection;
begin
  Result := Prop('UserName');
end;

function TMUserDictionary.Password: TLinqProjection;
begin
  Result := Prop('Password');
end;

function TMUserDictionary.AdressList: IMAdressDictionary;
begin
  Result := TMAdressDictionary.Create(PropName('AdressList'));
end;

function TMUserDictionary.PhoneList: IMPhoneDictionary;
begin
  Result := TMPhoneDictionary.Create(PropName('PhoneList'));
end;

function TMUserDictionary.MembersList: IMPersonDictionary;
begin
  Result := TMPersonDictionary.Create(PropName('MembersList'));
end;

function TMUserDictionary.EmailsList: IMEmailDictionary;
begin
  Result := TMEmailDictionary.Create(PropName('EmailsList'));
end;

function TMUserDictionary.WorkFor: IMPersonDictionary;
begin
  Result := TMPersonDictionary.Create(PropName('WorkFor'));
end;

function TMUserDictionary.Group: IMUserGroupDictionary;
begin
  Result := TMUserGroupDictionary.Create(PropName('Group'));
end;

{ TMUserGroupDictionary }

function TMUserGroupDictionary.Name: TLinqProjection;
begin
  Result := Prop('Name');
end;

function TMUserGroupDictionary.Id: TLinqProjection;
begin
  Result := Prop('Id');
end;

function TMUserGroupDictionary.CanCDatabases: TLinqProjection;
begin
  Result := Prop('CanCDatabases');
end;

function TMUserGroupDictionary.CanCreateDb: TLinqProjection;
begin
  Result := Prop('CanCreateDb');
end;

function TMUserGroupDictionary.CanChangeActiveDb: TLinqProjection;
begin
  Result := Prop('CanChangeActiveDb');
end;

function TMUserGroupDictionary.CanDeleteDb: TLinqProjection;
begin
  Result := Prop('CanDeleteDb');
end;

function TMUserGroupDictionary.CanCCat: TLinqProjection;
begin
  Result := Prop('CanCCat');
end;

function TMUserGroupDictionary.CanCUnit: TLinqProjection;
begin
  Result := Prop('CanCUnit');
end;

{ TDefaultDictionary }

function TDefaultDictionary.MAdress: IMAdressDictionary;
begin
  Result := TMAdressDictionary.Create;
end;

function TDefaultDictionary.MCat: IMCatDictionary;
begin
  Result := TMCatDictionary.Create;
end;

function TDefaultDictionary.MEmail: IMEmailDictionary;
begin
  Result := TMEmailDictionary.Create;
end;

function TDefaultDictionary.MPerson: IMPersonDictionary;
begin
  Result := TMPersonDictionary.Create;
end;

function TDefaultDictionary.MPhone: IMPhoneDictionary;
begin
  Result := TMPhoneDictionary.Create;
end;

function TDefaultDictionary.MUnit: IMUnitDictionary;
begin
  Result := TMUnitDictionary.Create;
end;

function TDefaultDictionary.MUser: IMUserDictionary;
begin
  Result := TMUserDictionary.Create;
end;

function TDefaultDictionary.MUserGroup: IMUserGroupDictionary;
begin
  Result := TMUserGroupDictionary.Create;
end;

end.
