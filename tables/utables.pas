{$M+}
unit utables;

interface

uses
  Aurelius.Types.Blob,
  Aurelius.Types.Nullable,
  Aurelius.Types.Proxy,
  Aurelius.Mapping.Attributes,
  Aurelius.Validation,
  Aurelius.Validation.Attributes,
  System.Generics.Collections,
  uconst,
  System.SysUtils,
  Aurelius.Engine.ObjectManager,
  Aurelius.Criteria.Base,
  Aurelius.Criteria.Linq;

type
  MTable = class
  protected
    FId: Integer;
  public
    property ID: Integer read FId write FId;
  published
    function getSlaves: TArray<MTable>; virtual; abstract;
    class function getAs<T: class>(Table: MTable): T;
    class function getClasses: TArray<TClass>;

  end;
  //*******************************************

  [Entity,
  Automapping]
  [Model(DB_LOCAL_PARAMS_NAME)]
  TMParamsLocal = class(MTable)
  private
    FId: Integer;
    FLanguage: string;
  public
    property ID: Integer read FId write FId;
  published
    property Language: string read FLanguage write FLanguage;
    function getSlaves: TArray<MTable>; override;
    //constructor Create(lang: string);
  end;

  [Entity,
  Automapping]
  [Model(DB_LOCAL_PARAMS_NAME)]
  TMLang = class(MTable)
  private
    FId: Integer;
    FLang: string;
  public
    property ID: Integer read FId write FId;
    property Lang: string read FLang write FLang;
    constructor Create(lang: string);
    function getSlaves: TArray<MTable>; override;
  end;

  [Entity,
  Automapping]
  [Model(DB_LOCAL_PARAMS_NAME)]
  TMWrod = class(MTable)
  private
    FWord: WideString;
    FId: Integer;
  public
    property ID: Integer read FId write FId;
  published
    property Word: WideString read FWord write FWord;
    constructor Create(Word: WideString);
    function getSlaves: TArray<MTable>; override;
  end;

  [Entity,
  Automapping]
  [Model(DB_LOCAL_PARAMS_NAME)]
  TMTrans = class(MTable)
  private
    FId: Integer;
    FTrans: WideString;
    FLang: TMLang;
    FWord: TMWrod;
  public
    property ID: Integer read FId write FId;
  published
    property Trans: WideString read FTrans write FTrans;
    property Lang: TMLang read Flang write Flang;
    property Word: TMWrod read FWord write FWord;
    constructor Create(Trans: WideString; Lang: TMLang; Word: TMWrod);
    function getSlaves: TArray<MTable>; override;
  end;

//*************************************

  [Entity,
  Automapping]
  [Model(DB_SERVER_PARAMS_NAME)]
  TMDatabase = class(MTable)
  private
    FId: Integer;
    FTitle: WideString;
    FDescrription: Nullable<WideString>;
    FIsDefaultDb: Boolean;
    function getName: string;

  public
    property ID: Integer read FId write FId;
  published
    property Title: WideString read FTitle write FTitle;
    property descrription: Nullable<WideString> read FDescrription write FDescrription;
    property isDefaultDb: Boolean read FIsDefaultDb write FIsDefaultDb default False;
    property name: string read getName;
    constructor Create(name: WideString); overload;

    function getSlaves: TArray<MTable>; override;
  published
   { published declarations }
  end;
 //******************************

  [Entity,
  Automapping]
  TMPhone = class(MTable)
  private
    FId: Integer;
    FTitle: WideString;
    FPhone: WideString;
    FRem: WideString;

  { private declarations }
  protected
  { protected declarations }
  public
  { public declarations }
    property ID: Integer read FId write FId;
  published
    property title: WideString read Ftitle write Ftitle;
    property phone: WideString read FPhone write FPhone;
    property rem: WideString read Frem write Frem;
    function getSlaves: TArray<MTable>; override;
  end;

//*****************************************************
  [Entity,
  Automapping]
  TMEmail = class(MTable)
  private
    FTitle: WideString;
    FEmail: WideString;
    FRem: WideString;
    FId: Integer;
  { private declarations }
  protected
  { protected declarations }
  public
  { public declarations }
    property ID: Integer read FId write FId;
  published
    property title: WideString read Ftitle write Ftitle;
    property email: WideString read FEmail write FEmail;
    property rem: WideString read Frem write Frem;
    function getSlaves: TArray<MTable>; override;
  published
  { published declarations }
  end;
//***************************

  [Entity,
  Automapping]
  TMAdress = class(MTable)
  private
    FTitle: Nullable<WideString>;
    FAdress: WideString;
    FRem: Nullable<WideString>;
    FId: Integer;
  { private declarations }
  protected
  { protected declarations }
  public
  { public declarations }
    property ID: Integer read FId write FId;
  published
    property title: Nullable<WideString> read Ftitle write Ftitle;
    property adress: WideString read FAdress write FAdress;
    property rem: Nullable<WideString> read Frem write Frem;
    constructor Create(adr: WideString; title: WideString = ''; rem: WideString
      = ''); overload;
    function getSlaves: TArray<MTable>; override;
  published
  { published declarations }
  end;
//*****************************************

  [Entity,
  Automapping]
  TMPerson = class(MTable)
  private
    FName: WideString;
    FAdressList: TList<TMAdress>;
    FPhoneList: TList<TMPhone>;
    FMembersList: TList<TMPerson>;
    FWebsite: Nullable<WideString>;
    FEmailsList: TList<TMEmail>;
    FRem: Nullable<WideString>;
    FWorkFor: Nullable<TMPerson>;
    FId: Integer;

  public
  { public declarations }
    property ID: Integer read FId write FId;
  published
    property name: WideString read FName write FName;
  public
    property adressList: TList<TMAdress> read FAdressList write FAdressList;
    property phoneList: TList<TMPhone> read FPhoneList write FPhoneList;
    property membersList: TList<TMPerson> read FMembersList write FMembersList;
    property emailsList: TList<TMEmail> read FEmailsList write FEmailsList;
  published
    property website: Nullable<WideString> read FWebsite write FWebsite;
    property rem: Nullable<WideString> read Frem write Frem;
    property workFor: Nullable<TMPerson> read FWorkFor write FWorkFor;
    constructor Create(name: WideString);
    destructor Destroy; override;
    procedure addAdress(var adr: TMAdress);
    procedure addEmail(var email: TMEmail);
    procedure addPhone(var phone: TMPhone);
    function getSlaves: TArray<MTable>; override;
  end;
  //**************************************

  [Entity,
  Automapping]
  [UniqueKey('name')]
  TMUserGroup = class(MTable)
  private
    FName: WideString;
    FId: Integer;
    FCanCDatabases: Boolean;
    FCanCreateDb: Boolean;
    FCanChangeActiveDb: Boolean;
    FCanDeleteDb: Boolean;
    FCanCCat: Boolean;
    FCanCUnit: Boolean;

  public
    property ID: Integer read FId write FId;
  published
    property name: WideString read FName write FName;
    property CanCDatabases: Boolean read FCanCDatabases write FCanCDatabases;
    property CanCreateDb: Boolean read FCanCreateDb write FCanCreateDb;
    property CanChangeActiveDb: Boolean read FCanChangeActiveDb write FCanChangeActiveDb;
    property CanDeleteDb: Boolean read FCanDeleteDb write FCanDeleteDb;
    property CanCCat: Boolean read FCanCCat write FCanCCat;
    property CanCUnit: Boolean read FCanCUnit write FCanCUnit;
    constructor Create(groupName: WideString); overload;
    procedure setAllpermissionsTo(permission: Boolean);
    class function getSuperGroup(var objMan: TObjectManager): TMUserGroup;
    class function createSuperGroup: TMUserGroup; static;
    function getSlaves: TArray<MTable>; override;
  end;
 //************************************

  [Entity,
  Automapping]
  [Inheritance(TInheritanceStrategy.JoinedTables)]
  TMUser = class(TMPerson)
  private
    FUserName: WideString;
    FPassword: WideString;
    FGroup: TMUserGroup;

  published
    property UserName: WideString read FUserName write FUserName;
    property Password: WideString read FPassword write FPassword;
  public
    property Group: TMUserGroup read FGroup write FGroup;

    constructor Create(name, userName, password: WideString; group: TMUserGroup);
      overload;
    destructor Destroy; override;
    class function createSuperUser: TMUser; static;
    function getSlaves: TArray<MTable>; override;
  end;
  //*****************

  [Entity,
  Automapping]
  TMUnit = class(MTable)
  private
    FName: WideString;
    FId: Integer;

  public
    property ID: Integer read FId write FId;
  published
    property name: WideString read FName write FName;
  end;
  //****************************

  [Entity,
  Automapping]
  TMCat = class(MTable)
  private
    FName: WideString;
    FId: Integer;
    FParent: TMCat;
    procedure SetParent(const Value: TMCat);
  public
    property ID: Integer read FId write FId;
  published
    property Name: WideString read FName write FName;
    property Parent: TMCat read FParent write setParent;
    constructor Create(name: WideString; Parent: TMCat = nil) overload;
  end;

implementation


{ TMDatabase }

constructor TMDatabase.Create(name: WideString);
begin
  inherited Create;
  Self.Title := name;
end;

function TMDatabase.getName: string;
begin
  Result := 'db_' + intToStr(Self.Id);
end;

function TMDatabase.getSlaves: TArray<MTable>;
begin
  Result := [];
end;

{ TMLang }

constructor TMLang.Create(lang: string);
begin
  Self.Lang := lang;
end;

{ RMTrans }

constructor TMTrans.Create(Trans: WideString; Lang: TMLang; Word: TMWrod);
begin
  Self.FTrans := Trans;
  Self.Lang := Lang;
  Self.Word := Word;
end;

function TMLang.getSlaves: TArray<MTable>;
begin
  Result := [];
end;

{ TMWrod }

constructor TMWrod.Create(word: WideString);
begin
  Self.Word := word;
end;

function TMWrod.getSlaves: TArray<MTable>;
begin
  Result := [];
end;

{ TMPerson }
procedure TMPerson.addAdress(var adr: TMAdress);
begin
  Self.adressList.Add(adr);
end;

procedure TMPerson.addEmail(var email: TMEmail);
begin
  Self.emailsList.Add(email);
end;

procedure TMPerson.addPhone(var phone: TMPhone);
begin
  Self.phoneList.Add(phone);
end;

function TMPerson.getSlaves: TArray<MTable>;
begin
  Result := [Self.workFor];
end;

constructor TMPerson.Create(name: WideString);
begin
  inherited Create;
  Self.name := name;
  Self.FadressList := TList<TMAdress>.Create;
  Self.FphoneList := TList<TMPhone>.Create;
  Self.FemailsList := TList<TMEmail>.Create;
  Self.FmembersList := TList<TMPerson>.Create;
  Self.website := SNull;
  Self.rem := SNull;
  Self.workFor := SNull;
end;

destructor TMPerson.Destroy;
begin
  Self.FadressList.Free;
  Self.FphoneList.Free;
  Self.FemailsList.Free;
  Self.FmembersList.Free;
  inherited;
end;

constructor TMUserGroup.Create(groupName: WideString);
begin
  inherited Create;
  Self.name := groupName;
end;

constructor TMUser.Create(name, userName, password: WideString; group: TMUserGroup);
begin
  inherited Create(name);
  Self.Name := name;
  Self.UserName := userName;
  Self.Password := password;
  Self.Group := group;
end;

class function TMUser.createSuperUser: TMUser;
begin
  try
    var grp := TMUserGroup.createSuperGroup;
    Result := TMUser.create(USER_SUPER_NAME, 'admin', '1234', grp);
  except
    raise;
  end;
end;

destructor TMUser.Destroy;
begin
  //self.userGroup.Free;
  inherited;
end;

function TMUser.getSlaves: TArray<MTable>;
begin
  Result := [Self.Group];
end;

constructor TMAdress.Create(adr, title, rem: WideString);
begin
  inherited Create;
  Assert(adr <> '');
  Self.adress := adr;
  Self.title := title;
  Self.rem := rem;
end;

class function TMUserGroup.createSuperGroup: TMUserGroup;
begin
  var grp := TMUserGroup.create(GROUP_SUPER_NAME);
  grp.setAllpermissionsTo(True);
  Result := grp;
end;

function TMUserGroup.getSlaves: TArray<MTable>;
begin
  Result := [];
end;

class function TMUserGroup.getSuperGroup(var objMan: TObjectManager): TMUserGroup;
begin
  result := objMan.Find<TMUserGroup>.Where(Linq['Name'] = GROUP_SUPER_NAME).UniqueResult;
end;

procedure TMUserGroup.setAllpermissionsTo(permission: Boolean);
begin
  Self.canCDatabases := permission;
  Self.canCreateDb := permission;
  Self.canChangeActiveDb := permission;
  Self.canDeleteDb := permission;
  Self.CanCCat := permission;
  Self.CanCUnit := permission;
end;

{ MTable }


{ MTable }

class function MTable.getAs<T>(table: MTable): T;
begin
  if Assigned(table) and (table.InheritsFrom(T)) then
    Result := T(table)
  else
    Result := nil;
end;

class function MTable.getClasses: TArray<TClass>;
begin
  Result := [TMUser, TMPerson, TMUserGroup];
end;


{ TMParamsLocal }

function TMParamsLocal.getSlaves: TArray<MTable>;
begin
  Result := [];
end;

function TMTrans.getSlaves: TArray<MTable>;
begin
  Result := [Self.Word, Self.Lang];
end;

{ TMPhone }

function TMPhone.getSlaves: TArray<MTable>;
begin
  Result := [];
end;

{ TMEmail }

function TMEmail.getSlaves: TArray<MTable>;
begin
  Result := [];
end;

function TMAdress.getSlaves: TArray<MTable>;
begin
  Result := [];
end;

{ TMCat }

constructor TMCat.Create(name: WideString; parent: TMCat);
begin

end;

procedure TMCat.SetParent(const Value: TMCat);
begin
  var aParent := Value;

  while aParent <> nil do
  begin
    if aParent.ID = Self.ID then
      raise Exception.Create('CANT BE SELF PARENT OR PARENT OF AN ANCECTER');
    aParent := aParent.Parent
  end;
  Self.FParent:= Value;

end;

initialization
  RegisterEntity(TMWrod);
  RegisterEntity(TMTrans);
  RegisterEntity(TMParamsLocal);
  RegisterEntity(TMUser);
  RegisterEntity(TMDatabase);
  RegisterEntity(TMUnit);
  RegisterEntity(TMCat);

end.

