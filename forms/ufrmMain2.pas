﻿unit ufrmMain2;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.Menus,
  Vcl.ToolWin,
  Vcl.ComCtrls,
  System.ImageList,
  Vcl.ImgList,
  SVGIconImageListBase,
  SVGIconImageList,
  System.Actions,
  Vcl.ActnList,
  uDbMasterSlaveCore,
  Vcl.ExtCtrls,
  uMPageControl,
  ufrmBase,
  uFrmUnit,
  DB,
  System.StrUtils,
  System.UITypes, Vcl.StdCtrls;

type
  TfrmMain2 = class(TForm)
    MainMenu: TMainMenu;
    ABLES1: TMenuItem;
    ActionList: TActionList;
    ImageList: TSVGIconImageList;
    actUnitOpen: TAction;
    actDbNew: TAction;
    ControlBar1: TControlBar;
    ToolBar2: TToolBar;
    ToolButton1: TToolButton;
    pagectrl: TMformPageControl;
    actDbLock: TAction;
    actDbSave: TAction;
    actDbCancel: TAction;
    actDbDelete: TAction;
    actDbFirst: TAction;
    actDbNext: TAction;
    actDbPrior: TAction;
    actDbLast: TAction;
    actDbSearch: TAction;
    actPageClose: TAction;
    ToolBar1: TToolBar;
    ToolButton3: TToolButton;
    barSearch: TControlBar;
    ToolBar4: TToolBar;
    Panel1: TPanel;
    edtSearch: TEdit;
    Label2: TLabel;
    Label1: TLabel;
    cmbSearchFileds: TComboBox;
    procedure actUnitOpenExecute(Sender: TObject);
    procedure actDbNewExecute(Sender: TObject);
    procedure pagectrlStateChange(Sender: TObject);
    procedure pagectrlLock(Sender: TObject; LockVal: Boolean);
    procedure actDbLockExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure actDbSaveExecute(Sender: TObject);
    procedure actDbCancelExecute(Sender: TObject);
    procedure actDbDeleteExecute(Sender: TObject);
    procedure actDbFirstExecute(Sender: TObject);
    procedure actDbNextExecute(Sender: TObject);
    procedure actDbPriorExecute(Sender: TObject);
    procedure actDbLastExecute(Sender: TObject);
    procedure actDbSearchExecute(Sender: TObject);
    procedure cmbSearchFiledsSelect(Sender: TObject);
    procedure edtSearchChange(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);

  private
    function getActiveForm: TfrmBase;
    function IsShortCut(var message: TWMKey): Boolean; overload;
    function getActiveManager: TMAsterSlaveManager;
    { Private declarations }
  public
    property activeForm: TfrmBase read getActiveForm;
    property activeManager: TMAsterSlaveManager read getActiveManager;
  end;

var
  frmMain2: TfrmMain2;

implementation

{$R *.dfm}

procedure TfrmMain2.actDbCancelExecute(Sender: TObject);
begin
  self.pagectrl.getActivePage.form.manager.cancel;
end;

procedure TfrmMain2.actDbDeleteExecute(Sender: TObject);
begin
  self.pagectrl.getActivePage.form.manager.delete;
end;

procedure TfrmMain2.actDbFirstExecute(Sender: TObject);
begin
  activeManager.first;
end;

procedure TfrmMain2.actDbLastExecute(Sender: TObject);
begin
  activeManager.last;
end;

procedure TfrmMain2.actDbLockExecute(Sender: TObject);
begin
  pagectrl.getActivePage.form.Manager.readOnly := not pagectrl.getActivePage.form.Manager.readonly;
end;

procedure TfrmMain2.actDbNewExecute(Sender: TObject);
begin
  Self.pageCtrl.getActivePage.form.Manager.append;
end;

procedure TfrmMain2.actDbNextExecute(Sender: TObject);
begin
  activeManager.next;
end;

procedure TfrmMain2.actDbPriorExecute(Sender: TObject);
begin
  activemanager.prior;
end;

procedure TfrmMain2.actDbSaveExecute(Sender: TObject);
begin
  self.pagectrl.getActivePage.form.manager.save;
end;

procedure TfrmMain2.actDbSearchExecute(Sender: TObject);
begin
  barSearch.Visible := not barSearch.Visible;
  if barSearch.Visible then
  begin
    cmbSearchFileds.Items.Clear;
    edtSearch.SelectAll;
    edtSearch.SetFocus;
    with activeManager.ActiveDatasource.DataSet do
    begin
    var fld:TField;
     for fld in Fields do
     begin
       cmbSearchFileds.Items.Add(fld.DisplayName);
     end;
     cmbSearchFileds.text:=activeform.activeDbControl.GetField.DisplayName
    end;
  end
  else
      begin
        activeManager.DataSourceMaster.DataSet.Filtered:=False;
        activeManager.DataSourceSlave.DataSet.Filtered:=false;
      end;
end;

procedure TfrmMain2.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
if TAction(Action).Category = 'DB' then
  TAction(Action).Enabled := pagectrl.PageCount>0;
Handled:=True;
end;

procedure TfrmMain2.actUnitOpenExecute(Sender: TObject);
begin
  var pg := TMformSheet.Create(pagectrl, TfrmUnit);
  pg.ImageIndex := TAction(Sender).ImageIndex;
  pagectrl.ActivePage := pg;

end;

procedure TfrmMain2.cmbSearchFiledsSelect(Sender: TObject);
begin
  edtSearch.setFocus;
end;

procedure TfrmMain2.edtSearchChange(Sender: TObject);
begin
  activeForm.manager.search(edtSearch.text,cmbSearchFileds.Text);
end;

procedure TfrmMain2.FormCreate(Sender: TObject);
begin
  actDbNext.ShortCut := ShortCut(VK_DOWN, []);
  actDbPrior.ShortCut := ShortCut(VK_UP, []);
  actDbFirst.ShortCut := ShortCut(VK_UP, [ssCtrl, ssAlt]);
  actDbLast.ShortCut := ShortCut(VK_DOWN, [ssCtrl, ssAlt]);
  actDbLock.ShortCut := ShortCut(vkL, [ssCtrl]);
  actDbSave.ShortCut := ShortCut(vkS, [ssCtrl]);
  actDbCancel.ShortCut := ShortCut(VK_ESCAPE, []);
  actDbSearch.ShortCut := ShortCut(vkF, [ssCtrl]);
  actDbNew.ShortCut := ShortCut(vkN, [ssCtrl]);
  actPageClose.ShortCut := ShortCut(vkQ, [ssCtrl]);
end;

function TfrmMain2.getActiveForm: TfrmBase;
begin
  result := TfrmBase(Self.pageCtrl.ActivePage.Controls[0])
end;

function TfrmMain2.getActiveManager: TMAsterSlaveManager;
begin
  result := activeForm.manager;
end;

procedure TfrmMain2.pagectrlLock(Sender: TObject; LockVal: Boolean);
begin
  if LockVal then
  begin
    if not ContainsText(pagectrl.getActivePage.stateCaption, '🔏') then
      pagectrl.getActivePage.stateCaption := pagectrl.getActivePage.stateCaption + '🔏';
  end
  else
  begin
    pagectrl.getActivePage.stateCaption := ReplaceText(pagectrl.getActivePage.stateCaption,
      '🔏', '');
  end;
end;

procedure TfrmMain2.pagectrlStateChange(Sender: TObject);
begin
  var datasource := TDataSource(Sender);
  if (datasource.state = dsEdit) or (datasource.state = dsInsert) then
  begin
    if not ContainsText(pagectrl.getActivePage.stateCaption, '✏...') then
      pagectrl.getActivePage.stateCaption := pagectrl.getActivePage.stateCaption + '✏...';
    self.pagectrl.getActivePage.Changed := True;
  end
  else
  begin
    pagectrl.getActivePage.stateCaption := ReplaceText(pagectrl.getActivePage.stateCaption,
      '✏...', '');
    self.pagectrl.getActivePage.Changed := false;
  end;

end;

function TfrmMain2.IsShortCut(var Message: TWMKey): Boolean;

  function DispatchShortCut(const Owner: TComponent): Boolean;
  begin
    Result := False;
    { Dispatch to all children }
    if self.PageCtrl.PageCount = 0 then
    begin
      Result := False;
    end
    else
    begin
      var actList := self.ActionList;
      if actList.IsShortCut(Message) then
      begin
        Result := True;
        Exit;
      end;
    end;
  end;

begin
  Result := DispatchShortCut(Self);
  if not Result then
    Result := inherited;
end;

end.

