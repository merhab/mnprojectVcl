object frmMain: TfrmMain
  Left = 0
  Top = 0
  AlphaBlendValue = 200
  BiDiMode = bdLeftToRight
  Caption = 'MAIN'
  ClientHeight = 488
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  Menu = MainMenu1
  ParentBiDiMode = False
  Position = poDesktopCenter
  StyleName = 'Windows'
  OnClose = FormClose
  TextHeight = 15
  inline framePageCtrl: TframePageCtrl
    Left = 0
    Top = 61
    Width = 624
    Height = 427
    Align = alClient
    TabOrder = 0
    ExplicitTop = 61
    ExplicitWidth = 624
    ExplicitHeight = 427
    inherited pageCtrl: TAdvPageControl
      Width = 624
      Height = 427
      ExplicitWidth = 624
      ExplicitHeight = 427
    end
    inherited ActionList1: TActionList
      Left = 484
      Top = 130
      inherited actUnitOpen: TAction
        ImageIndex = -1
        ImageName = 'frames\kilogram-svgrepo-com'
      end
      inherited actDbFirst: TAction
        Hint = 'GO FIRST'
        ImageIndex = -1
        ImageName = 'DB\first'
      end
      inherited actDbNext: TAction
        Hint = 'GO NEXT'
        ImageIndex = -1
        ImageName = 'DB\play-svgrepo-com'
      end
      inherited actDbPrior: TAction
        Hint = 'GO PRIOR'
        ImageIndex = -1
        ImageName = 'DB\prior'
      end
      inherited actDbLast: TAction
        Hint = 'GO LAST'
        ImageIndex = -1
        ImageName = 'DB\end'
      end
      inherited actDbLock: TAction
        Hint = 'LOCK/UNLOCK'
        ImageIndex = -1
        ImageName = 'DB\lock-svgrepo-com'
      end
      inherited actDbSave: TAction
        Hint = 'SAVE'
        ImageIndex = -1
        ImageName = 'DB\save-floppy-svgrepo-com'
      end
      inherited actDbCancel: TAction
        Hint = 'CANCEL'
        ImageIndex = -1
        ImageName = 'DB\rewind-15-seconds-forward-svgrepo-com'
      end
      inherited actDbSearch: TAction
        Hint = 'SEARCH'
        ImageIndex = -1
        ImageName = 'DB\search-alt-2-svgrepo-com'
      end
      inherited actDbNew: TAction
        Hint = 'NEW'
        ImageIndex = -1
        ImageName = 'DB\add-svgrepo-com'
      end
      inherited actCatOpen: TAction
        ImageIndex = -1
        ImageName = 'frames\category-svgrepo-com'
      end
      inherited actPageClose: TAction
        Caption = 'CLOSE CURRENT PAGE'
        ImageIndex = 15
        ImageName = 'search-alt-2-svgrepo-com'
      end
    end
  end
  object ControlBar1: TControlBar
    Left = 0
    Top = 0
    Width = 624
    Height = 38
    Align = alTop
    AutoSize = True
    TabOrder = 1
    object ToolBar2: TToolBar
      Left = 11
      Top = 2
      Width = 275
      Height = 30
      AutoSize = True
      ButtonHeight = 30
      ButtonWidth = 30
      Caption = 'ToolBar2'
      TabOrder = 0
      object ToolButton1: TToolButton
        Left = 0
        Top = 0
        Action = framePageCtrl.actDbFirst
        ParentShowHint = False
        ShowHint = True
      end
      object ToolButton2: TToolButton
        Left = 30
        Top = 0
        Action = framePageCtrl.actDbPrior
        ParentShowHint = False
        ShowHint = True
      end
      object ToolButton3: TToolButton
        Left = 60
        Top = 0
        Action = framePageCtrl.actDbNext
        ParentShowHint = False
        ShowHint = True
      end
      object ToolButton4: TToolButton
        Left = 90
        Top = 0
        Action = framePageCtrl.actDbLast
        ParentShowHint = False
        ShowHint = True
      end
      object ToolButton5: TToolButton
        Left = 120
        Top = 0
        Action = framePageCtrl.actDbNew
        ParentShowHint = False
        ShowHint = True
      end
      object ToolButton10: TToolButton
        Left = 150
        Top = 0
        Action = framePageCtrl.actDbSave
        ParentShowHint = False
        ShowHint = True
      end
      object ToolButton9: TToolButton
        Left = 180
        Top = 0
        Action = framePageCtrl.actDbCancel
        ParentShowHint = False
        ShowHint = True
      end
      object ToolButton6: TToolButton
        Left = 210
        Top = 0
        Action = framePageCtrl.actDbSearch
        ParentShowHint = False
        ShowHint = True
      end
      object ToolButton8: TToolButton
        Left = 240
        Top = 0
        Action = framePageCtrl.actDbLock
        ParentShowHint = False
        ShowHint = True
      end
    end
    object ToolBar3: TToolBar
      Left = 299
      Top = 2
      Width = 150
      Height = 30
      AutoSize = True
      ButtonHeight = 30
      ButtonWidth = 30
      Caption = 'ToolBar3'
      TabOrder = 1
      object ToolButton7: TToolButton
        Left = 0
        Top = 0
        Action = framePageCtrl.actCatOpen
        ParentShowHint = False
        ShowHint = True
        OnClick = ToolButton7Click
      end
      object ToolButton11: TToolButton
        Left = 30
        Top = 0
        Action = framePageCtrl.actUnitOpen
        ParentShowHint = False
        ShowHint = True
        OnClick = ToolButton11Click
      end
      object ToolButton12: TToolButton
        Left = 60
        Top = 0
        Width = 8
        Caption = 'ToolButton12'
        ImageName = 'frames\category-svgrepo-com'
        Style = tbsSeparator
      end
      object ToolButton13: TToolButton
        Left = 68
        Top = 0
        Action = framePageCtrl.actPageClose
        ParentShowHint = False
        ShowHint = True
      end
    end
  end
  object edit: TDBAdvLUEdit
    Left = 0
    Top = 38
    Width = 624
    Height = 23
    EmptyTextStyle = []
    FlatLineColor = 11250603
    FocusColor = clWindow
    FocusFontColor = 3881787
    LabelFont.Charset = DEFAULT_CHARSET
    LabelFont.Color = clWindowText
    LabelFont.Height = -12
    LabelFont.Name = 'Segoe UI'
    LabelFont.Style = []
    Lookup.Font.Charset = DEFAULT_CHARSET
    Lookup.Font.Color = clWindowText
    Lookup.Font.Height = -11
    Lookup.Font.Name = 'Segoe UI'
    Lookup.Font.Style = []
    Lookup.Separator = ';'
    Align = alTop
    Color = clWindow
    TabOrder = 2
    Text = 'edit'
    Visible = True
    Version = '1.4.1.0'
    AutoHistory = False
    AutoSynchronize = False
    FileLookup = False
    LookupPersist.Enable = False
    LookupPersist.Location = plInifile
    LookupPersist.Key = 'LUEdit'
    LookupPersist.Section = 'Values'
    LookupPersist.Count = 0
    LookupPersist.MaxCount = False
    MatchCase = False
    DataLookup = False
  end
  object MainMenu1: TMainMenu
    Left = 416
    Top = 144
    object ALES1: TMenuItem
      Caption = 'TALES'
      object ALES2: TMenuItem
        Action = framePageCtrl.actUnitOpen
        ImageName = 'category-list-solid-svgrepo-com'
      end
    end
  end
end
