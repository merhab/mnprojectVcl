unit ufrmLogin;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.StdCtrls,
  ufrmLoginCtrl,
  Aurelius.Dictionary.Classes;

type
  TfrmLogin = class(TForm)
    GroupBox1: TGroupBox;
    txtUser: TEdit;
    txtPasss: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    btnLogin: TButton;
    Button1: TButton;
    Button3: TButton;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure btnLoginClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLogin: TfrmLogin;

implementation

{$R *.dfm}

uses
  ufrmCon,
  utables,
  uglobalLogin,
  Aurelius.Criteria.Linq;

procedure TfrmLogin.btnLoginClick(Sender: TObject);
begin
  if Login(txtUser.Text, txtPasss.Text) then
    ShowMessage('loged:' + db.Title + ':' + db.name + ':' + user.UserName + '/'
      + user.Password + '/' + user.Group.name);
end;

procedure TfrmLogin.Button1Click(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfrmLogin.Button3Click(Sender: TObject);
begin
  init;
end;

end.

