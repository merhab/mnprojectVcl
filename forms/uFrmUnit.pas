unit uFrmUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ufrmBase, Data.DB, Vcl.Grids,
  Vcl.DBGrids, Vcl.StdCtrls, Vcl.Mask, Vcl.ExtCtrls, Vcl.DBCtrls, udbControls,
  Aurelius.Bind.BaseDataset, Aurelius.Bind.Dataset, uStdCtrls;

type
  TfrmUnit = class(TfrmBase)
    datasetMaster: TAureliusDataset;
    datasetMastername: TWideStringField;
    datasetSlave: TAureliusDataset;
    WideStringField2: TWideStringField;
    AureliusEntityField3: TAureliusEntityField;
    AureliusEntityField4: TAureliusEntityField;
    WideStringField3: TWideStringField;
    pnlControls: TGroupBox;
    Label1: TLabel;
    edtName: TMDbEdit;
    pnlGrid: TGroupBox;
    DBGrid1: TDBGrid;
    procedure FormCreate(Sender: TObject);
    destructor destroy;override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmUnit: TfrmUnit;

implementation

{$R *.dfm}
uses ufrmCon,uglobalLogin,Aurelius.Criteria.Base,utables;
destructor TfrmUnit.destroy;
begin
  datasetMaster.Manager.Free;
  datasetSlave.Manager.Free;
  inherited;
end;

procedure TfrmUnit.FormCreate(Sender: TObject);
begin
  inherited;
  var Manager := frmcon.createObjectManagerServer(Db.name);
  critMaster := Manager.Find<TMUnit>;
  var critMaster:TCriteria := TCriteria(self.critMaster);
  datasetMaster.SetSourceCriteria(critMaster.Clone);
  datasetMaster.Manager := Manager;
  datasetMaster.Open;
end;

end.
