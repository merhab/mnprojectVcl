unit ufrmBase;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.StdCtrls,
  uDbMasterSlaveCore,
  uStdCtrls,
  Data.DB,
  uInterfaces;

type
  TfrmBase = class(TForm)
    grpMaster: TMGroupBox;
    grpSlave: TMGroupBox;
    dtsMaster: TMDatasource;
    dtsSlave: TMDatasource;
  private
    FactiveDbControl: Ieditable;
    { Private declarations }
    procedure ActiveControlChanged(Sender: TObject);
    function getActiveDbControl: Ieditable;
  public
    { Public declarations }
    Manager: TMAsterSlaveManager;
    property activeDbControl: Ieditable read getActiveDbControl write FactiveDbControl;
    constructor Create(AOwner: TComponent); override;
    destructor destroy; override;
    var
      critMaster: TObject;
  end;

var
  frmBase: TfrmBase;

implementation

{$R *.dfm}

{ TfrmBase }

procedure TfrmBase.ActiveControlChanged(Sender: TObject);
begin
  if Supports(Self.ActiveControl, Ieditable) then
    factiveDbControl := (activeControl as Ieditable);
end;

constructor TfrmBase.Create(AOwner: TComponent);
begin
  inherited;
  manager := TMAsterSlaveManager.create(dtsMaster, dtsSlave, grpMaster, grpSlave);
end;

destructor TfrmBase.destroy;
begin
  manager.free;
  critMaster.Free;
  inherited;
end;

function getFirstIeditableControl(parent: TwinControl): Ieditable;
begin
  result := nil;
  if supports(parent, Ieditable) then
  begin
    result := parent as ieditable;
    Exit;
  end;

  var i: Integer;
  for i := 0 to parent.controlCount - 1 do
  begin
    if parent.controls[i] is TWinControl then
      result := getFirstIeditableControl(TWinControl(parent.controls[i]));
    if result <> nil then
      Exit;
  end;

end;

function TfrmBase.getActiveDbControl: Ieditable;
begin
  if FactiveDbControl <> nil then
    Result := FactiveDbControl
  else
  begin
    result := getFirstIeditableControl(grpMaster);
  end;
end;

end.

