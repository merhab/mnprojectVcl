object frmMain2: TfrmMain2
  Left = 0
  Top = 0
  Caption = 'frmMain2'
  ClientHeight = 441
  ClientWidth = 770
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  Menu = MainMenu
  OnCreate = FormCreate
  TextHeight = 15
  object ControlBar1: TControlBar
    Left = 0
    Top = 0
    Width = 770
    Height = 38
    Align = alTop
    AutoSize = True
    TabOrder = 0
    object ToolBar2: TToolBar
      Left = 11
      Top = 2
      Width = 150
      Height = 30
      AutoSize = True
      ButtonHeight = 30
      ButtonWidth = 30
      Caption = 'ToolBar2'
      Images = imageList
      TabOrder = 0
      object ToolButton1: TToolButton
        Left = 0
        Top = 0
        Action = actDbNew
        ParentShowHint = False
        ShowHint = True
      end
    end
    object ToolBar1: TToolBar
      Left = 174
      Top = 2
      Width = 150
      Height = 30
      AutoSize = True
      ButtonHeight = 30
      ButtonWidth = 30
      Caption = 'ToolBar3'
      Images = imageList
      TabOrder = 1
      object ToolButton3: TToolButton
        Left = 0
        Top = 0
        Action = actUnitOpen
        ParentShowHint = False
        ShowHint = True
      end
    end
  end
  object pagectrl: TMformPageControl
    Left = 0
    Top = 69
    Width = 770
    Height = 372
    Align = alClient
    Images = imageList
    OwnerDraw = True
    TabOrder = 1
    TabWidth = 150
    OnStateChange = pagectrlStateChange
  end
  object barSearch: TControlBar
    Left = 0
    Top = 38
    Width = 770
    Height = 31
    Align = alTop
    AutoSize = True
    TabOrder = 2
    Visible = False
    object ToolBar4: TToolBar
      Left = 174
      Top = 2
      Width = 507
      Height = 23
      AutoSize = True
      ButtonHeight = 23
      ButtonWidth = 30
      Caption = 'ToolBar3'
      Images = imageList
      TabOrder = 0
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 497
        Height = 23
        BevelOuter = bvNone
        TabOrder = 0
        object Label2: TLabel
          Left = 288
          Top = 2
          Width = 34
          Height = 15
          Caption = 'Label2'
        end
        object Label1: TLabel
          Left = 15
          Top = 2
          Width = 34
          Height = 15
          Caption = 'Label1'
        end
        object edtSearch: TEdit
          Left = 55
          Top = -2
          Width = 227
          Height = 23
          TabOrder = 0
          OnChange = edtSearchChange
        end
        object cmbSearchFileds: TComboBox
          Left = 344
          Top = -2
          Width = 145
          Height = 23
          TabOrder = 1
          Text = 'ComboBox1'
          OnSelect = cmbSearchFiledsSelect
        end
      end
    end
  end
  object MainMenu: TMainMenu
    Left = 488
    Top = 200
    object ABLES1: TMenuItem
      Caption = 'TABLES'
    end
  end
  object ActionList: TActionList
    Images = imageList
    OnUpdate = ActionListUpdate
    Left = 496
    Top = 96
    object actUnitOpen: TAction
      Category = 'FORMS'
      Caption = 'UNITES'
      ImageIndex = 11
      ImageName = 'measure-technical-drawing-svgrepo-com'
      ShortCut = 49218
      OnExecute = actUnitOpenExecute
    end
    object actDbNew: TAction
      Category = 'DB'
      Caption = 'NEW'
      Hint = 'NEW RECORD'
      ImageIndex = 1
      ImageName = 'add-svgrepo-com'
      OnExecute = actDbNewExecute
    end
    object actDbLock: TAction
      Category = 'DB'
      Caption = 'LOCK'
      ImageIndex = 10
      ImageName = 'lock-svgrepo-com'
      ShortCut = 16460
      OnExecute = actDbLockExecute
    end
    object actDbSave: TAction
      Category = 'DB'
      Caption = 'SAVE'
      ImageIndex = 16
      ImageName = 'save-floppy-svgrepo-com'
      OnExecute = actDbSaveExecute
    end
    object actDbCancel: TAction
      Category = 'DB'
      Caption = 'CANCEL'
      ImageIndex = 14
      ImageName = 'refresh-svgrepo-com'
      OnExecute = actDbCancelExecute
    end
    object actDbDelete: TAction
      Category = 'DB'
      Caption = 'DELETE'
      ImageIndex = 4
      ImageName = 'delete-svgrepo-com'
      OnExecute = actDbDeleteExecute
    end
    object actDbFirst: TAction
      Category = 'DB'
      Caption = 'FIRST'
      ImageIndex = 6
      ImageName = 'first'
      OnExecute = actDbFirstExecute
    end
    object actDbNext: TAction
      Category = 'DB'
      Caption = 'NEXT'
      ImageIndex = 12
      ImageName = 'play-svgrepo-com'
      OnExecute = actDbNextExecute
    end
    object actDbPrior: TAction
      Category = 'DB'
      Caption = 'PRIOR'
      ImageIndex = 13
      ImageName = 'prior'
      OnExecute = actDbPriorExecute
    end
    object actDbLast: TAction
      Category = 'DB'
      Caption = 'LAST'
      ImageIndex = 5
      ImageName = 'end'
      OnExecute = actDbLastExecute
    end
    object actDbSearch: TAction
      Category = 'DB'
      Caption = 'SEARCH'
      ImageIndex = 17
      ImageName = 'search-alt-2-svgrepo-com'
      OnExecute = actDbSearchExecute
    end
    object actPageClose: TAction
      Category = 'FORMS'
      Caption = 'CLOSE'
      ImageIndex = 3
      ImageName = 'close-ellipse-svgrepo-com'
    end
  end
  object imageList: TSVGIconImageList
    SVGIconItems = <
      item
        IconName = 'add-circle-svgrepo-com'
        SVGText = 
          '<?xml version="1.0" encoding="utf-8"?><!-- Uploaded to: SVG Repo' +
          ', www.svgrepo.com, Generator: SVG Repo Mixer Tools -->'#13#10'<svg wid' +
          'th="800px" height="800px" viewBox="0 0 24 24" fill="none" xmlns=' +
          '"http://www.w3.org/2000/svg">'#13#10'<path d="M15 12L12 12M12 12L9 12M' +
          '12 12L12 9M12 12L12 15" stroke="#1C274C" stroke-width="1.5" stro' +
          'ke-linecap="round"/>'#13#10'<path d="M7 3.33782C8.47087 2.48697 10.178' +
          '6 2 12 2C17.5228 2 22 6.47715 22 12C22 17.5228 17.5228 22 12 22C' +
          '6.47715 22 2 17.5228 2 12C2 10.1786 2.48697 8.47087 3.33782 7" s' +
          'troke="#1C274C" stroke-width="1.5" stroke-linecap="round"/>'#13#10'</s' +
          'vg>'
      end
      item
        IconName = 'add-svgrepo-com'
        SVGText = 
          '<?xml version="1.0" ?>'#13#10#13#10'<!-- Uploaded to: SVG Repo, www.svgrep' +
          'o.com, Generator: SVG Repo Mixer Tools -->'#13#10'<svg width="800px" h' +
          'eight="800px" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/' +
          'svg">'#13#10#13#10'<title/>'#13#10#13#10'<g id="Complete">'#13#10#13#10'<g data-name="add" id=' +
          '"add-2">'#13#10#13#10'<g>'#13#10#13#10'<line fill="none" stroke="#000000" stroke-lin' +
          'ecap="round" stroke-linejoin="round" stroke-width="2" x1="12" x2' +
          '="12" y1="19" y2="5"/>'#13#10#13#10'<line fill="none" stroke="#000000" str' +
          'oke-linecap="round" stroke-linejoin="round" stroke-width="2" x1=' +
          '"5" x2="19" y1="12" y2="12"/>'#13#10#13#10'</g>'#13#10#13#10'</g>'#13#10#13#10'</g>'#13#10#13#10'</svg>'
      end
      item
        IconName = 'category-svgrepo-com'
        SVGText = 
          '<?xml version="1.0" encoding="utf-8"?>'#10#13'<!-- Uploaded to: SVG Re' +
          'po, www.svgrepo.com, Generator: SVG Repo Mixer Tools -->'#10'<svg wi' +
          'dth="800px" height="800px" viewBox="0 0 48 48" xmlns="http://www' +
          '.w3.org/2000/svg">'#13#10'  <title>category</title>'#13#10'  <g id="Layer_2"' +
          ' data-name="Layer 2">'#13#10'    <g id="invisible_box" data-name="invi' +
          'sible box">'#13#10'      <rect width="48" height="48" fill="none"/>'#13#10' ' +
          '   </g>'#13#10'    <g id="icons_Q2" data-name="icons Q2">'#13#10'      <path' +
          ' d="M24,7.7,29.3,16H18.6L24,7.7M24,2a2.1,2.1,0,0,0-1.7,1L13.2,17' +
          'a2.3,2.3,0,0,0,0,2,1.9,1.9,0,0,0,1.7,1H33a2.1,2.1,0,0,0,1.7-1,1.' +
          '8,1.8,0,0,0,0-2l-9-14A1.9,1.9,0,0,0,24,2Z"/>'#13#10'      <path d="M43' +
          ',43H29a2,2,0,0,1-2-2V27a2,2,0,0,1,2-2H43a2,2,0,0,1,2,2V41A2,2,0,' +
          '0,1,43,43ZM31,39H41V29H31Z"/>'#13#10'      <path d="M13,28a6,6,0,1,1-6' +
          ',6,6,6,0,0,1,6-6m0-4A10,10,0,1,0,23,34,10,10,0,0,0,13,24Z"/>'#13#10'  ' +
          '  </g>'#13#10'  </g>'#13#10'</svg>'
      end
      item
        IconName = 'close-ellipse-svgrepo-com'
        SVGText = 
          '<?xml version="1.0" encoding="utf-8"?><!-- Uploaded to: SVG Repo' +
          ', www.svgrepo.com, Generator: SVG Repo Mixer Tools -->'#13#10'<svg wid' +
          'th="800px" height="800px" viewBox="0 0 24 24" fill="none" xmlns=' +
          '"http://www.w3.org/2000/svg">'#13#10'<path d="M8.00386 9.41816C7.61333' +
          ' 9.02763 7.61334 8.39447 8.00386 8.00395C8.39438 7.61342 9.02755' +
          ' 7.61342 9.41807 8.00395L12.0057 10.5916L14.5907 8.00657C14.9813' +
          ' 7.61605 15.6144 7.61605 16.0049 8.00657C16.3955 8.3971 16.3955 ' +
          '9.03026 16.0049 9.42079L13.4199 12.0058L16.0039 14.5897C16.3944 ' +
          '14.9803 16.3944 15.6134 16.0039 16.0039C15.6133 16.3945 14.9802 ' +
          '16.3945 14.5896 16.0039L12.0057 13.42L9.42097 16.0048C9.03045 16' +
          '.3953 8.39728 16.3953 8.00676 16.0048C7.61624 15.6142 7.61624 14' +
          '.9811 8.00676 14.5905L10.5915 12.0058L8.00386 9.41816Z" fill="#0' +
          'F0F0F"/>'#13#10'<path fill-rule="evenodd" clip-rule="evenodd" d="M23 1' +
          '2C23 18.0751 18.0751 23 12 23C5.92487 23 1 18.0751 1 12C1 5.9248' +
          '7 5.92487 1 12 1C18.0751 1 23 5.92487 23 12ZM3.00683 12C3.00683 ' +
          '16.9668 7.03321 20.9932 12 20.9932C16.9668 20.9932 20.9932 16.96' +
          '68 20.9932 12C20.9932 7.03321 16.9668 3.00683 12 3.00683C7.03321' +
          ' 3.00683 3.00683 7.03321 3.00683 12Z" fill="#0F0F0F"/>'#13#10'</svg>'
      end
      item
        IconName = 'delete-svgrepo-com'
        SVGText = 
          '<?xml version="1.0" encoding="utf-8"?><!-- Uploaded to: SVG Repo' +
          ', www.svgrepo.com, Generator: SVG Repo Mixer Tools -->'#13#10'<svg wid' +
          'th="800px" height="800px" viewBox="0 0 24 24" fill="none" xmlns=' +
          '"http://www.w3.org/2000/svg">'#13#10'<path d="M10 11V17" stroke="#0000' +
          '00" stroke-width="2" stroke-linecap="round" stroke-linejoin="rou' +
          'nd"/>'#13#10'<path d="M14 11V17" stroke="#000000" stroke-width="2" str' +
          'oke-linecap="round" stroke-linejoin="round"/>'#13#10'<path d="M4 7H20"' +
          ' stroke="#000000" stroke-width="2" stroke-linecap="round" stroke' +
          '-linejoin="round"/>'#13#10'<path d="M6 7H12H18V18C18 19.6569 16.6569 2' +
          '1 15 21H9C7.34315 21 6 19.6569 6 18V7Z" stroke="#000000" stroke-' +
          'width="2" stroke-linecap="round" stroke-linejoin="round"/>'#13#10'<pat' +
          'h d="M9 5C9 3.89543 9.89543 3 11 3H13C14.1046 3 15 3.89543 15 5V' +
          '7H9V5Z" stroke="#000000" stroke-width="2" stroke-linecap="round"' +
          ' stroke-linejoin="round"/>'#13#10'</svg>'
      end
      item
        IconName = 'end'
        SVGText = 
          '<?xml version="1.0" encoding="UTF-8" standalone="no"?>'#10'<!-- Uplo' +
          'aded to: SVG Repo, www.svgrepo.com, Generator: SVG Repo Mixer To' +
          'ols -->'#10#10'<svg'#10'   width="800px"'#10'   height="800px"'#10'   viewBox="-0.' +
          '5 0 25 25"'#10'   fill="none"'#10'   version="1.1"'#10'   id="svg2"'#10'   sodip' +
          'odi:docname="end.svg"'#10'   inkscape:version="1.3 (0e150ed, 2023-07' +
          '-21)"'#10'   xmlns:inkscape="http://www.inkscape.org/namespaces/inks' +
          'cape"'#10'   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sod' +
          'ipodi-0.dtd"'#10'   xmlns="http://www.w3.org/2000/svg"'#10'   xmlns:svg=' +
          '"http://www.w3.org/2000/svg">'#10'  <defs'#10'     id="defs2" />'#10'  <sodi' +
          'podi:namedview'#10'     id="namedview2"'#10'     pagecolor="#ffffff"'#10'   ' +
          '  bordercolor="#000000"'#10'     borderopacity="0.25"'#10'     inkscape:' +
          'showpageshadow="2"'#10'     inkscape:pageopacity="0.0"'#10'     inkscape' +
          ':pagecheckerboard="0"'#10'     inkscape:deskcolor="#d1d1d1"'#10'     ink' +
          'scape:zoom="0.295"'#10'     inkscape:cx="398.30508"'#10'     inkscape:cy' +
          '="400"'#10'     inkscape:window-width="1280"'#10'     inkscape:window-he' +
          'ight="449"'#10'     inkscape:window-x="222"'#10'     inkscape:window-y="' +
          '34"'#10'     inkscape:window-maximized="0"'#10'     inkscape:current-lay' +
          'er="svg2" />'#10'  <path'#10'     d="m 4,21.3199 c -2.55,-0.88 -3,-5.6 -' +
          '3,-8.91 0,-3.30998 0.43,-7.99989 3,-8.89989 2.71,-0.93 10.99997,' +
          '5.1499 10.99997,8.89989 0,3.75 -8.28997,9.84 -10.99997,8.91 z"'#10' ' +
          '    stroke="#000000"'#10'     stroke-width="1.5"'#10'     stroke-linecap' +
          '="round"'#10'     stroke-linejoin="round"'#10'     id="path1" />'#10'  <path' +
          #10'     d="m 23.0195,18.92 c 0,1.3808 -1.11929,2.5 -2.5,2.5 -1.380' +
          '71,0 -2.5,-1.1192 -2.5,-2.5 V 5.92004 c 0,-1.38071 1.11929,-2.5 ' +
          '2.5,-2.5 1.38071,0 2.5,1.11929 2.5,2.5 z"'#10'     stroke="#000000"'#10 +
          '     stroke-width="1.5"'#10'     stroke-linecap="round"'#10'     stroke-' +
          'linejoin="round"'#10'     id="path2" />'#10'</svg>'#10
      end
      item
        IconName = 'first'
        SVGText = 
          '<?xml version="1.0" encoding="UTF-8" standalone="no"?>'#10'<!-- Uplo' +
          'aded to: SVG Repo, www.svgrepo.com, Generator: SVG Repo Mixer To' +
          'ols -->'#10#10'<svg'#10'   width="800px"'#10'   height="800px"'#10'   viewBox="-0.' +
          '5 0 25 25"'#10'   fill="none"'#10'   version="1.1"'#10'   id="svg2"'#10'   sodip' +
          'odi:docname="first.svg"'#10'   inkscape:version="1.3 (0e150ed, 2023-' +
          '07-21)"'#10'   xmlns:inkscape="http://www.inkscape.org/namespaces/in' +
          'kscape"'#10'   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/s' +
          'odipodi-0.dtd"'#10'   xmlns="http://www.w3.org/2000/svg"'#10'   xmlns:sv' +
          'g="http://www.w3.org/2000/svg">'#10'  <defs'#10'     id="defs2" />'#10'  <so' +
          'dipodi:namedview'#10'     id="namedview2"'#10'     pagecolor="#ffffff"'#10' ' +
          '    bordercolor="#000000"'#10'     borderopacity="0.25"'#10'     inkscap' +
          'e:showpageshadow="2"'#10'     inkscape:pageopacity="0.0"'#10'     inksca' +
          'pe:pagecheckerboard="0"'#10'     inkscape:deskcolor="#d1d1d1"'#10'     i' +
          'nkscape:zoom="0.295"'#10'     inkscape:cx="398.30508"'#10'     inkscape:' +
          'cy="400"'#10'     inkscape:window-width="1280"'#10'     inkscape:window-' +
          'height="449"'#10'     inkscape:window-x="222"'#10'     inkscape:window-y' +
          '="34"'#10'     inkscape:window-maximized="0"'#10'     inkscape:current-l' +
          'ayer="svg2" />'#10'  <path'#10'     d="m 20.0195,21.3199 c 2.55,-0.88 3,' +
          '-5.6 3,-8.91 0,-3.30998 -0.43,-7.99989 -3,-8.89989 -2.71,-0.93 -' +
          '10.99997,5.1499 -10.99997,8.89989 0,3.75 8.28997,9.84 10.99997,8' +
          '.91 z"'#10'     stroke="#000000"'#10'     stroke-width="1.5"'#10'     stroke' +
          '-linecap="round"'#10'     stroke-linejoin="round"'#10'     id="path1" />' +
          #10'  <path'#10'     d="m 1,18.92 c 0,1.3808 1.11929,2.5 2.5,2.5 1.3807' +
          '1,0 2.5,-1.1192 2.5,-2.5 V 5.92004 c 0,-1.38071 -1.11929,-2.5 -2' +
          '.5,-2.5 -1.38071,0 -2.5,1.11929 -2.5,2.5 z"'#10'     stroke="#000000' +
          '"'#10'     stroke-width="1.5"'#10'     stroke-linecap="round"'#10'     strok' +
          'e-linejoin="round"'#10'     id="path2" />'#10'</svg>'#10
      end
      item
        IconName = 'kilogram-svgrepo-com'
        SVGText = 
          '<?xml version="1.0" encoding="iso-8859-1"?>'#13#10'<!-- Uploaded to: S' +
          'VG Repo, www.svgrepo.com, Generator: SVG Repo Mixer Tools -->'#13#10'<' +
          'svg fill="#000000" height="800px" width="800px" version="1.1" id' +
          '="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http:/' +
          '/www.w3.org/1999/xlink" '#13#10#9' viewBox="0 0 490 490" xml:space="pre' +
          'serve">'#13#10'<g>'#13#10#9'<path d="M398.08,115.327h-84.256c1.323-5.403,2.04' +
          '-11.042,2.04-16.847c0-39.074-31.789-70.864-70.864-70.864'#13#10#9#9'c-39' +
          '.075,0-70.869,31.789-70.869,70.864c0,5.805,0.717,11.444,2.04,16.' +
          '847H91.919L0,462.384h490L398.08,115.327z M245,47.782'#13#10#9#9'c27.953,' +
          '0,50.698,22.745,50.698,50.698c0,27.954-22.745,50.699-50.698,50.6' +
          '99c-27.959,0-50.704-22.745-50.704-50.699'#13#10#9#9'C194.296,70.527,217.' +
          '041,47.782,245,47.782z M107.437,135.492h77.157c12.48,20.291,34.8' +
          '87,33.852,60.405,33.852'#13#10#9#9'c25.518,0,47.923-13.562,60.401-33.852' +
          'h77.161L463.8,442.219H26.201L107.437,135.492z"/>'#13#10#9'<polygon poin' +
          'ts="235.331,237.559 213.033,237.559 165.462,288.258 165.462,237.' +
          '559 147.919,237.559 147.919,341.479 '#13#10#9#9'165.462,341.479 165.462,' +
          '312.341 180.622,295.989 215.114,341.479 235.924,341.479 193.555,' +
          '283.056 '#9'"/>'#13#10#9'<path d="M324.534,319.478c-6.642,4.259-14.894,6.3' +
          '95-24.754,6.395c-9.861,0-18.257-3.346-25.189-10.036'#13#10#9#9'c-6.945-6' +
          '.69-10.416-15.558-10.416-26.61c0-11.053,3.568-19.975,10.707-26.7' +
          '62c7.139-6.788,15.958-10.187,26.46-10.187'#13#10#9#9'c5.456,0,10.235,0.6' +
          '72,14.349,2.007c4.114,1.337,8.203,3.745,12.268,7.211l9.365-13.38' +
          '1c-9.909-8.717-22.346-13.079-37.312-13.079'#13#10#9#9's-27.73,5.154-38.2' +
          '8,15.456c-10.563,10.313-15.837,23.192-15.837,38.655c0,15.462,5.1' +
          '77,28.249,15.534,38.358'#13#10#9#9'c10.356,10.108,23.217,15.165,38.582,1' +
          '5.165c18.232,0,32.254-5.402,42.067-16.206v-35.533h-17.543V319.47' +
          '8z"/>'#13#10'</g>'#13#10'</svg>'
      end
      item
        IconName = 'kilo-svgrepo-com'
        SVGText = 
          '<?xml version="1.0" encoding="iso-8859-1"?>'#13#10'<!-- Uploaded to: S' +
          'VG Repo, www.svgrepo.com, Generator: SVG Repo Mixer Tools -->'#13#10'<' +
          '!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org' +
          '/Graphics/SVG/1.1/DTD/svg11.dtd">'#13#10'<svg fill="#000000" height="8' +
          '00px" width="800px" version="1.1" id="Capa_1" xmlns="http://www.' +
          'w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" '#13#10#9' ' +
          'viewBox="0 0 302.033 302.033" xml:space="preserve">'#13#10'<g>'#13#10#9'<path' +
          ' d="M245.1,100.362h-50.988c-9.772,0-17.858-7.141-19.379-16.485c1' +
          '2.939-7.984,21.584-22.287,21.584-38.576'#13#10#9#9'C196.316,20.322,175.9' +
          '96,0,151.018,0c-24.979,0-45.301,20.322-45.301,45.301c0,16.289,8.' +
          '645,30.592,21.585,38.576'#13#10#9#9'c-1.52,9.344-9.605,16.485-19.378,16.' +
          '485H56.935L31.43,302.033h239.174L245.1,100.362z M151.018,24.066'#13 +
          #10#9#9'c11.708,0,21.232,9.526,21.232,21.234c0,11.708-9.525,21.233-21' +
          '.232,21.233c-11.708,0-21.234-9.525-21.234-21.233'#13#10#9#9'C129.783,33.' +
          '592,139.309,24.066,151.018,24.066z M142.106,225.464l-19.979-29.7' +
          '89l-5.76,6.66v23.129h-7.83v-60.658h7.83v29.249'#13#10#9#9'h0.27c1.62-2.3' +
          '4,3.24-4.5,4.77-6.48l18.54-22.769h9.72l-21.96,25.739l23.669,34.9' +
          '19H142.106z M193.766,193.875v25.289'#13#10#9#9'c0,9.99-1.98,16.109-6.21,' +
          '19.889c-4.229,3.96-10.35,5.22-15.839,5.22c-5.22,0-10.979-1.26-14' +
          '.489-3.6l1.98-6.03'#13#10#9#9'c2.88,1.8,7.38,3.42,12.779,3.42c8.1,0,14.0' +
          '4-4.23,14.04-15.21v-4.86h-0.18c-2.43,4.05-7.109,7.29-13.859,7.29' +
          #13#10#9#9'c-10.799,0-18.539-9.179-18.539-21.239c0-14.76,9.629-23.129,1' +
          '9.619-23.129c7.56,0,11.7,3.96,13.589,7.56h0.18l0.36-6.57h6.93'#13#10#9 +
          #9'C193.946,185.056,193.766,188.565,193.766,193.875z"/>'#13#10#9'<path d=' +
          '"M174.417,187.126c-7.56,0-12.96,6.39-12.96,16.469c0,8.55,4.32,15' +
          '.659,12.869,15.659c4.86,0,9.27-3.06,10.979-8.1'#13#10#9#9'c0.45-1.35,0.6' +
          '3-2.88,0.63-4.229v-7.83c0-1.35-0.09-2.52-0.45-3.6C184.046,190.90' +
          '5,180.177,187.126,174.417,187.126z"/>'#13#10'</g>'#13#10'</svg>'
      end
      item
        IconName = 'lock-slash-svgrepo-com'
        SVGText = 
          '<?xml version="1.0" encoding="utf-8"?><!-- Uploaded to: SVG Repo' +
          ', www.svgrepo.com, Generator: SVG Repo Mixer Tools -->'#13#10'<svg wid' +
          'th="800px" height="800px" viewBox="0 0 24 24" fill="none" xmlns=' +
          '"http://www.w3.org/2000/svg">'#13#10'<path d="M3 3L21 21M17 10V8C17 5.' +
          '23858 14.7614 3 12 3C11.0283 3 10.1213 3.27719 9.35386 3.75681M7' +
          '.08383 7.08338C7.02878 7.38053 7 7.6869 7 8V10.0288M19.5614 19.5' +
          '618C19.273 20.0348 18.8583 20.4201 18.362 20.673C17.7202 21 16.8' +
          '802 21 15.2 21H8.8C7.11984 21 6.27976 21 5.63803 20.673C5.07354 ' +
          '20.3854 4.6146 19.9265 4.32698 19.362C4 18.7202 4 17.8802 4 16.2' +
          'V14.8C4 13.1198 4 12.2798 4.32698 11.638C4.6146 11.0735 5.07354 ' +
          '10.6146 5.63803 10.327C5.99429 10.1455 6.41168 10.0647 7 10.0288' +
          'M19.9998 14.4023C19.9978 12.9831 19.9731 12.227 19.673 11.638C19' +
          '.3854 11.0735 18.9265 10.6146 18.362 10.327C17.773 10.0269 17.01' +
          '69 10.0022 15.5977 10.0002M10 10H8.8C8.05259 10 7.47142 10 7 10.' +
          '0288" stroke="#000000" stroke-width="2" stroke-linecap="round" s' +
          'troke-linejoin="round"/>'#13#10'</svg>'
      end
      item
        IconName = 'lock-svgrepo-com'
        SVGText = 
          '<?xml version="1.0" encoding="utf-8"?><!-- Uploaded to: SVG Repo' +
          ', www.svgrepo.com, Generator: SVG Repo Mixer Tools -->'#13#10'<svg wid' +
          'th="800px" height="800px" viewBox="0 0 24 24" fill="none" xmlns=' +
          '"http://www.w3.org/2000/svg">'#13#10'<path d="M7 10.0288C7.47142 10 8.' +
          '05259 10 8.8 10H15.2C15.9474 10 16.5286 10 17 10.0288M7 10.0288C' +
          '6.41168 10.0647 5.99429 10.1455 5.63803 10.327C5.07354 10.6146 4' +
          '.6146 11.0735 4.32698 11.638C4 12.2798 4 13.1198 4 14.8V16.2C4 1' +
          '7.8802 4 18.7202 4.32698 19.362C4.6146 19.9265 5.07354 20.3854 5' +
          '.63803 20.673C6.27976 21 7.11984 21 8.8 21H15.2C16.8802 21 17.72' +
          '02 21 18.362 20.673C18.9265 20.3854 19.3854 19.9265 19.673 19.36' +
          '2C20 18.7202 20 17.8802 20 16.2V14.8C20 13.1198 20 12.2798 19.67' +
          '3 11.638C19.3854 11.0735 18.9265 10.6146 18.362 10.327C18.0057 1' +
          '0.1455 17.5883 10.0647 17 10.0288M7 10.0288V8C7 5.23858 9.23858 ' +
          '3 12 3C14.7614 3 17 5.23858 17 8V10.0288" stroke="#000000" strok' +
          'e-width="2" stroke-linecap="round" stroke-linejoin="round"/>'#13#10'</' +
          'svg>'
      end
      item
        IconName = 'measure-technical-drawing-svgrepo-com'
        SVGText = 
          '<?xml version="1.0" encoding="utf-8"?>'#13#10#13#10'<!-- Uploaded to: SVG ' +
          'Repo, www.svgrepo.com, Generator: SVG Repo Mixer Tools -->'#10'<svg ' +
          'version="1.1" id="Icons" xmlns="http://www.w3.org/2000/svg" xmln' +
          's:xlink="http://www.w3.org/1999/xlink" '#13#10#9' viewBox="0 0 32 32" x' +
          'ml:space="preserve">'#13#10'<style type="text/css">'#13#10#9'.st0{fill:none;s' +
          'troke:#000000;stroke-width:2;stroke-linecap:round;stroke-linejoi' +
          'n:round;stroke-miterlimit:10;}'#13#10#9'.st1{fill:none;stroke:#000000;s' +
          'troke-width:2;stroke-linecap:round;stroke-linejoin:round;}'#13#10#9'.st' +
          '2{fill:none;stroke:#000000;stroke-width:2;stroke-linecap:round;s' +
          'troke-linejoin:round;stroke-dasharray:6,6;}'#13#10#9'.st3{fill:none;str' +
          'oke:#000000;stroke-width:2;stroke-linecap:round;stroke-linejoin:' +
          'round;stroke-dasharray:4,4;}'#13#10#9'.st4{fill:none;stroke:#000000;str' +
          'oke-width:2;stroke-linecap:round;}'#13#10#9'.st5{fill:none;stroke:#0000' +
          '00;stroke-width:2;stroke-linecap:round;stroke-dasharray:3.1081,3' +
          '.1081;}'#13#10#9#13#10#9#9'.st6{fill:none;stroke:#000000;stroke-width:2;strok' +
          'e-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;strok' +
          'e-dasharray:4,3;}'#13#10'</style>'#13#10'<polygon class="st0" points="3,3 3,' +
          '27 29,27 "/>'#13#10'<polygon class="st0" points="8,15 8,22 16,22 "/>'#13#10 +
          '<polygon class="st0" points="29,13.7 29,16.5 26.2,16.5 15.6,5.9 ' +
          '18.4,3.1 "/>'#13#10'</svg>'
      end
      item
        IconName = 'play-svgrepo-com'
        SVGText = 
          '<?xml version="1.0" encoding="utf-8"?><!-- Uploaded to: SVG Repo' +
          ', www.svgrepo.com, Generator: SVG Repo Mixer Tools -->'#13#10'<svg wid' +
          'th="800px" height="800px" viewBox="0 0 24 24" fill="none" xmlns=' +
          '"http://www.w3.org/2000/svg">'#13#10'<path d="M20.4086 9.35258C22.5305' +
          ' 10.5065 22.5305 13.4935 20.4086 14.6474L7.59662 21.6145C5.53435' +
          ' 22.736 3 21.2763 3 18.9671L3 5.0329C3 2.72368 5.53435 1.26402 7' +
          '.59661 2.38548L20.4086 9.35258Z" stroke="#1C274C" stroke-width="' +
          '1.5"/>'#13#10'</svg>'
      end
      item
        IconName = 'prior'
        SVGText = 
          '<?xml version="1.0" encoding="UTF-8" standalone="no"?>'#10'<!-- Uplo' +
          'aded to: SVG Repo, www.svgrepo.com, Generator: SVG Repo Mixer To' +
          'ols -->'#10#10'<svg'#10'   width="800px"'#10'   height="800px"'#10'   viewBox="0 0' +
          ' 24 24"'#10'   fill="none"'#10'   version="1.1"'#10'   id="svg1"'#10'   sodipodi' +
          ':docname="prior.svg"'#10'   inkscape:version="1.3 (0e150ed, 2023-07-' +
          '21)"'#10'   xmlns:inkscape="http://www.inkscape.org/namespaces/inksc' +
          'ape"'#10'   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodi' +
          'podi-0.dtd"'#10'   xmlns="http://www.w3.org/2000/svg"'#10'   xmlns:svg="' +
          'http://www.w3.org/2000/svg">'#10'  <defs'#10'     id="defs1" />'#10'  <sodip' +
          'odi:namedview'#10'     id="namedview1"'#10'     pagecolor="#ffffff"'#10'    ' +
          ' bordercolor="#000000"'#10'     borderopacity="0.25"'#10'     inkscape:s' +
          'howpageshadow="2"'#10'     inkscape:pageopacity="0.0"'#10'     inkscape:' +
          'pagecheckerboard="0"'#10'     inkscape:deskcolor="#d1d1d1"'#10'     inks' +
          'cape:zoom="0.295"'#10'     inkscape:cx="398.30508"'#10'     inkscape:cy=' +
          '"398.30508"'#10'     inkscape:window-width="1496"'#10'     inkscape:wind' +
          'ow-height="820"'#10'     inkscape:window-x="0"'#10'     inkscape:window-' +
          'y="34"'#10'     inkscape:window-maximized="1"'#10'     inkscape:current-' +
          'layer="svg1" />'#10'  <path'#10'     d="m 4.5576337,9.35258 c -2.1219,1.' +
          '15392 -2.1219,4.14092 0,5.29482 l 12.8119803,6.9671 c 2.06227,1.' +
          '1215 4.59662,-0.3382 4.59662,-2.6474 V 5.0329 c 0,-2.30922 -2.53' +
          '435,-3.76888 -4.59661,-2.64742 z"'#10'     stroke="#1c274c"'#10'     str' +
          'oke-width="1.5"'#10'     id="path1" />'#10'</svg>'#10
      end
      item
        IconName = 'refresh-svgrepo-com'
        SVGText = 
          '<?xml version="1.0" encoding="utf-8"?><!-- Uploaded to: SVG Repo' +
          ', www.svgrepo.com, Generator: SVG Repo Mixer Tools -->'#13#10'<svg wid' +
          'th="800px" height="800px" viewBox="0 0 24 24" fill="none" xmlns=' +
          '"http://www.w3.org/2000/svg">'#13#10'<path d="M3.67981 11.3333H2.92981' +
          'H3.67981ZM3.67981 13L3.15157 13.5324C3.44398 13.8225 3.91565 13.' +
          '8225 4.20805 13.5324L3.67981 13ZM5.88787 11.8657C6.18191 11.574 ' +
          '6.18377 11.0991 5.89203 10.8051C5.60029 10.511 5.12542 10.5092 4' +
          '.83138 10.8009L5.88787 11.8657ZM2.52824 10.8009C2.2342 10.5092 1' +
          '.75933 10.511 1.46759 10.8051C1.17585 11.0991 1.17772 11.574 1.4' +
          '7176 11.8657L2.52824 10.8009ZM18.6156 7.39279C18.8325 7.74565 19' +
          '.2944 7.85585 19.6473 7.63892C20.0001 7.42199 20.1103 6.96007 19' +
          '.8934 6.60721L18.6156 7.39279ZM12.0789 2.25C7.03155 2.25 2.92981' +
          ' 6.3112 2.92981 11.3333H4.42981C4.42981 7.15072 7.84884 3.75 12.' +
          '0789 3.75V2.25ZM2.92981 11.3333L2.92981 13H4.42981L4.42981 11.33' +
          '33H2.92981ZM4.20805 13.5324L5.88787 11.8657L4.83138 10.8009L3.15' +
          '157 12.4676L4.20805 13.5324ZM4.20805 12.4676L2.52824 10.8009L1.4' +
          '7176 11.8657L3.15157 13.5324L4.20805 12.4676ZM19.8934 6.60721C18' +
          '.287 3.99427 15.3873 2.25 12.0789 2.25V3.75C14.8484 3.75 17.2727' +
          ' 5.20845 18.6156 7.39279L19.8934 6.60721Z" fill="#1C274C"/>'#13#10'<pa' +
          'th d="M20.3139 11L20.8411 10.4666C20.549 10.1778 20.0788 10.1778' +
          ' 19.7867 10.4666L20.3139 11ZM18.1004 12.1333C17.8058 12.4244 17.' +
          '8031 12.8993 18.0942 13.1939C18.3854 13.4885 18.8603 13.4913 19.' +
          '1549 13.2001L18.1004 12.1333ZM21.4729 13.2001C21.7675 13.4913 22' +
          '.2424 13.4885 22.5335 13.1939C22.8247 12.8993 22.822 12.4244 22.' +
          '5274 12.1332L21.4729 13.2001ZM5.31794 16.6061C5.1004 16.2536 4.6' +
          '383 16.1442 4.28581 16.3618C3.93331 16.5793 3.82391 17.0414 4.04' +
          '144 17.3939L5.31794 16.6061ZM11.8827 21.75C16.9451 21.75 21.0639' +
          ' 17.6915 21.0639 12.6667H19.5639C19.5639 16.8466 16.1332 20.25 1' +
          '1.8827 20.25V21.75ZM21.0639 12.6667V11H19.5639V12.6667H21.0639ZM' +
          '19.7867 10.4666L18.1004 12.1333L19.1549 13.2001L20.8411 11.5334L' +
          '19.7867 10.4666ZM19.7867 11.5334L21.4729 13.2001L22.5274 12.1332' +
          'L20.8411 10.4666L19.7867 11.5334ZM4.04144 17.3939C5.65405 20.007' +
          ' 8.56403 21.75 11.8827 21.75V20.25C9.10023 20.25 6.66584 18.7903' +
          ' 5.31794 16.6061L4.04144 17.3939Z" fill="#1C274C"/>'#13#10'</svg>'
      end
      item
        IconName = 'rewind-15-seconds-forward-svgrepo-com'
        SVGText = 
          '<?xml version="1.0" encoding="UTF-8" standalone="no"?>'#10'<!-- Uplo' +
          'aded to: SVG Repo, www.svgrepo.com, Generator: SVG Repo Mixer To' +
          'ols -->'#10#10'<svg'#10'   width="800px"'#10'   height="800px"'#10'   viewBox="0 0' +
          ' 24 24"'#10'   fill="none"'#10'   version="1.1"'#10'   id="svg3"'#10'   sodipodi' +
          ':docname="rewind-15-seconds-forward-svgrepo-com.svg"'#10'   inkscape' +
          ':version="1.3 (0e150ed, 2023-07-21)"'#10'   xmlns:inkscape="http://w' +
          'ww.inkscape.org/namespaces/inkscape"'#10'   xmlns:sodipodi="http://s' +
          'odipodi.sourceforge.net/DTD/sodipodi-0.dtd"'#10'   xmlns="http://www' +
          '.w3.org/2000/svg"'#10'   xmlns:svg="http://www.w3.org/2000/svg">'#10'  <' +
          'defs'#10'     id="defs3" />'#10'  <sodipodi:namedview'#10'     id="namedview' +
          '3"'#10'     pagecolor="#ffffff"'#10'     bordercolor="#000000"'#10'     bord' +
          'eropacity="0.25"'#10'     inkscape:showpageshadow="2"'#10'     inkscape:' +
          'pageopacity="0.0"'#10'     inkscape:pagecheckerboard="0"'#10'     inksca' +
          'pe:deskcolor="#d1d1d1"'#10'     inkscape:zoom="0.295"'#10'     inkscape:' +
          'cx="398.30508"'#10'     inkscape:cy="400"'#10'     inkscape:window-width' +
          '="1280"'#10'     inkscape:window-height="449"'#10'     inkscape:window-x' +
          '="222"'#10'     inkscape:window-y="34"'#10'     inkscape:window-maximize' +
          'd="0"'#10'     inkscape:current-layer="svg3" />'#10'  <path'#10'     d="M10 ' +
          '4.5L12 2C6.47715 2 2 6.47715 2 12C2 12.6849 2.06886 13.3538 2.20' +
          '004 14M16 2.83209C19.5318 4.3752 22 7.89936 22 12C22 17.5228 17.' +
          '5228 22 12 22C8.72852 22 5.82443 20.4287 4 18"'#10'     stroke="#1C2' +
          '74C"'#10'     stroke-width="1.5"'#10'     stroke-linecap="round"'#10'     st' +
          'roke-linejoin="round"'#10'     id="path3" />'#10'</svg>'#10
      end
      item
        IconName = 'save-floppy-svgrepo-com'
        SVGText = 
          '<?xml version="1.0" encoding="UTF-8" standalone="no"?>'#13#10'<!-- Upl' +
          'oaded to: SVG Repo, www.svgrepo.com, Generator: SVG Repo Mixer T' +
          'ools -->'#13#10'<svg width="800px" height="800px" viewBox="0 0 32 32" ' +
          'version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="ht' +
          'tp://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancod' +
          'ing.com/sketch/ns">'#13#10'    '#13#10'    <title>save-floppy</title>'#13#10'    <' +
          'desc>Created with Sketch Beta.</desc>'#13#10'    <defs>'#13#10#13#10'</defs>'#13#10'  ' +
          '  <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill' +
          '-rule="evenodd" sketch:type="MSPage">'#13#10'        <g id="Icon-Set" ' +
          'sketch:type="MSLayerGroup" transform="translate(-152.000000, -51' +
          '5.000000)" fill="#000000">'#13#10'            <path d="M171,525 C171.5' +
          '52,525 172,524.553 172,524 L172,520 C172,519.447 171.552,519 171' +
          ',519 C170.448,519 170,519.447 170,520 L170,524 C170,524.553 170.' +
          '448,525 171,525 L171,525 Z M182,543 C182,544.104 181.104,545 180' +
          ',545 L156,545 C154.896,545 154,544.104 154,543 L154,519 C154,517' +
          '.896 154.896,517 156,517 L158,517 L158,527 C158,528.104 158.896,' +
          '529 160,529 L176,529 C177.104,529 178,528.104 178,527 L178,517 L' +
          '180,517 C181.104,517 182,517.896 182,519 L182,543 L182,543 Z M16' +
          '0,517 L176,517 L176,526 C176,526.553 175.552,527 175,527 L161,52' +
          '7 C160.448,527 160,526.553 160,526 L160,517 L160,517 Z M180,515 ' +
          'L156,515 C153.791,515 152,516.791 152,519 L152,543 C152,545.209 ' +
          '153.791,547 156,547 L180,547 C182.209,547 184,545.209 184,543 L1' +
          '84,519 C184,516.791 182.209,515 180,515 L180,515 Z" id="save-flo' +
          'ppy" sketch:type="MSShapeGroup">'#13#10#13#10'</path>'#13#10'        </g>'#13#10'    <' +
          '/g>'#13#10'</svg>'
      end
      item
        IconName = 'search-alt-2-svgrepo-com'
        SVGText = 
          '<?xml version="1.0" encoding="utf-8"?><!-- Uploaded to: SVG Repo' +
          ', www.svgrepo.com, Generator: SVG Repo Mixer Tools -->'#13#10'<svg wid' +
          'th="800px" height="800px" viewBox="0 0 24 24" fill="none" xmlns=' +
          '"http://www.w3.org/2000/svg">'#13#10'<path d="M11 6C13.7614 6 16 8.238' +
          '58 16 11M16.6588 16.6549L21 21M19 11C19 15.4183 15.4183 19 11 19' +
          'C6.58172 19 3 15.4183 3 11C3 6.58172 6.58172 3 11 3C15.4183 3 19' +
          ' 6.58172 19 11Z" stroke="#000000" stroke-width="2" stroke-lineca' +
          'p="round" stroke-linejoin="round"/>'#13#10'</svg>'
      end>
    FixedColor = clGreen
    Scaled = True
    Left = 400
    Top = 208
  end
end
