unit ufrmLoginCtrl;

interface

uses
  ufrmCon, System.IOUtils, uconst, System.SysUtils, Vcl.Dialogs, ulocalizer,
  utables, Aurelius.Engine.ObjectManager, Aurelius.Criteria.Base,Aurelius.Criteria.Linq,
   System.UITypes;

procedure createDirectories;

procedure createLangs;

procedure init;

function createDb(title: string): string;

function Login(UserName, password: string): Boolean;

implementation

uses
  uglobalLogin;

function Login(UserName, password: string): Boolean;
begin
  Result := false;
  if user <> nil then
    user.group.free;
  user.free;
  db.free;
  if UserName = '' then
  begin
    MessageDlg(TR(ERR_USER_NAME), mtError, [mbOK], 0);
    Exit;
  end;
  var Manager := frmCon.createObjectManagerServerParams;
  try
    db := Manager.Find<TMDatabase>.Where(linq['IsDefaultDb'] = True).UniqueResult;
    if db = nil then
    begin
      MessageDlg(TR(ERR_NO_ACTIVE_DB), mtError, [mbOK], 0);
      Exit;
    end
    else
      Manager.Evict(db);
  finally
    Manager.Free;
  end;
  Manager := frmCon.createObjectManagerServer(db.name);
  try
    user := Manager.Find<TMUser>.Where(linq['UserName'] = UserName).Where(linq['Password'] = password).UniqueResult;
    if user = nil then
    begin
      MessageDlg(TR(ERR_NO_PERMISSION), mtError, [mbOK], 0);
      Exit;
    end
    else
    begin
      Manager.Evict(user);
      Manager.Evict(User.Group);
      Result := True;
    end;
  finally
    Manager.Free;
  end;
end;

//*********************************************

function createDb(title: string): string;
begin
  var db: TMDatabase;
  var objMan: TObjectManager;
  try
  //create dblist add default db to the list and make it default
    objMan := frmCon.createObjectManagerServerParams;
    try
      db := TMDatabase.Create(title);
      objMan.AddOwnership(db);
      db.isDefaultDb := True;
      try
        objMan.saveIfNotExists(db,linq['Title'] = title);
      except
        raise;
      end;
      result := db.name;
  //***********************
      frmCon.createDbServer(db.name);
      var dbMan := frmCon.createDbManagerServer(db.name);
      try
        dbMan.UpdateDatabase;
      finally
        dbMan.Free;
      end;

    finally
      objMan.Free;
    end;
  except
    on e: Exception do
    begin
      raise Exception.Create('cant create databases ' + title + N_LINE + e.ToString);
    end;

  end;
end;

procedure init;
begin
  try
    createDirectories;  //create dir
    if not frmCon.canConnectToServer.success then     //check conn
      raise Exception.Create(TR(ERR_CANT_CONNECT_TO_SERVER));
  except
    on E: Exception do
      MessageDlg(E.ToString, mtError, [mbOK], 0);
  end;
    //*************************************************************

  try
    //create local DAtabases param
    frmCon.setDatabaseLocal(DB_LOCAL_PARAMS_NAME);
    var dbMan := frmCon.createDbManagerLocalParams;
    try
      dbMan.UpdateDatabase;
    finally
      dbMan.Free;
    end;

  //************
    try
      frmCon.createDbServerParams; //create server params db
      dbMan := frmCon.createDbManagerServerParams;
      try
        dbMan.UpdateDatabase;
      finally
        dbMan.Free;
      end;
    except
      on E: Exception do
        MessageDlg(TR(ERR_CANT_CREATE_DB_SERVER) + N_LINE + E.ToString, mtError, [mbOK], 0);
    end;

    //create langs
    createLangs;
  except
    on e: Exception do
      MessageDlg(e.ToString, mtError, [mbOK], 0);
  end;
  var objMan: TObjectManager;
  try

  //create default database ; params database ;
    var dbName := createDb(DB_DEFAULT_NAME);
//    var dbMan := frmCon.createDbManagerServer(dbName);
//    try
//      dbMan.UpdateDatabase;
//    finally
//      dbMan.free;
//    end;
 //**********************
  //create super user with admin group
    objMan := frmCon.createObjectManagerServer(dbName);
    try
      var usr := TMUser.createSuperUser;
      objMan.AddOwnershipWithSlaves(usr);
      objMan.saveIfNotExists(usr, linq['Name'] = usr.name);
    finally
      objMan.Free;
    end;

  //************************************************************
  except
    on e: Exception do
    begin
      raise Exception.Create('cant create initial database/super user ' + DB_DEFAULT_NAME + '/' + USER_SUPER_NAME + N_LINE + e.ToString);
    end;
  end;
end;

procedure createDirectories;
begin
  var path := TPath.Combine(TPath.GetHomePath, APPDATA_DIR);
  if not TDirectory.Exists(path) then
  try
    TDirectory.CreateDirectory(path);
  except
    on e: Exception do
      MessageDlg(TR(ERR_CANT_CREATE_DIR) + N_LINE + E.ToString, mtError, [mbOK], 0);
  end;
end;

procedure createLangs;
begin
  var objMan := frmCon.createObjectManagerLocalParams;
  var transaction := objMan.Connection.BeginTransaction;
  try
    var lang := TMLang.Create(TRANS_DEFAULT_LANG);
    objMan.AddOwnership(lang);
    objMan.saveIfNotExists<TMLang>(lang,linq['Lang'] = TRANS_DEFAULT_LANG);
    lang := TMLang.Create(TRANS_ARABIC_LANG);
    objMan.AddOwnership(lang);
    objMan.saveIfNotExists(lang,linq['Lang'] = TRANS_ARABIC_LANG);
    lang := TMLang.Create(TRANS_FRENSH_LANG);
    objMan.AddOwnership(lang);
    objMan.saveIfNotExists(lang, linq['Lang'] = TRANS_FRENSH_LANG);
    var params := TMParamsLocal.Create;
    objMan.AddOwnership(params);
    //set default language  en
    params.Language := TRANS_DEFAULT_LANG;
    objMan.saveIfNotExists(params, linq['Language'] = TRANS_DEFAULT_LANG);
    transaction.Commit;
    objMan.Free;
  except
    on e: Exception do
    begin
      transaction.Rollback;
      objMan.Free;
      raise Exception.Create('cant create language data ' + N_LINE + e.ToString);
    end;

  end;

end;

end.

