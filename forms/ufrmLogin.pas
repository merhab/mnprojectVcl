unit ufrmLogin;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.StdCtrls,
  ufrmLoginCtrl,
  Aurelius.Dictionary.Classes,ufrmtest;

type
  TfrmLogin = class(TForm)
    GroupBox1: TGroupBox;
    txtUser: TEdit;
    txtPasss: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    btnLogin: TButton;
    Button1: TButton;
    Button3: TButton;
    Button2: TButton;
    Button4: TButton;
    procedure Button1Click(Sender: TObject);
    procedure btnLoginClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure txtPasssKeyPress(Sender: TObject; var Key: Char);
    procedure txtUserKeyPress(Sender: TObject; var Key: Char);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLogin: TfrmLogin;

implementation

{$R *.dfm}

uses
  ufrmCon,
  utables,
  uglobalLogin,
  Aurelius.Criteria.Linq, ufrmMain2;

procedure TfrmLogin.btnLoginClick(Sender: TObject);
begin
  if Login(txtUser.Text, txtPasss.Text) then
  begin
//    var frm := TForm1.Create(self);
    frmMain2.show;
  end;
end;

procedure TfrmLogin.Button1Click(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfrmLogin.Button3Click(Sender: TObject);
begin
  init;
end;

procedure TfrmLogin.Button4Click(Sender: TObject);
begin
  var frm:=tfrmtest.create(application);
  frm.show;
end;

procedure TfrmLogin.txtPasssKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #12 then
  begin
    btnLogin.Click;
    Key := #0;
  end;
end;

procedure TfrmLogin.txtUserKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
  begin
    txtPasss.SetFocus;
    Key := #0;
  end;
end;

end.

