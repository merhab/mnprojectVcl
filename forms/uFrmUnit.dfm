inherited frmUnit: TfrmUnit
  Caption = 'UNITES'
  StyleElements = [seFont, seClient, seBorder]
  OnCreate = FormCreate
  TextHeight = 15
  inherited grpMaster: TMGroupBox
    object pnlControls: TGroupBox
      Left = 2
      Top = 17
      Width = 620
      Height = 65
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 18
        Top = 27
        Width = 32
        Height = 15
        Caption = 'UNITE'
      end
      object edtName: TMDbEdit
        Left = 64
        Top = 24
        Width = 193
        Height = 23
        DataField = 'name'
        DataSource = dtsMaster
        TabOrder = 0
      end
    end
    object pnlGrid: TGroupBox
      Left = 2
      Top = 82
      Width = 620
      Height = 229
      Align = alClient
      Caption = 'UNITE LIST'
      TabOrder = 1
      object DBGrid1: TDBGrid
        Left = 2
        Top = 17
        Width = 616
        Height = 210
        Align = alClient
        DataSource = dtsMaster
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'name'
            Visible = True
          end>
      end
    end
  end
  inherited dtsMaster: TMDatasource
    DataSet = datasetMaster
  end
  inherited dtsSlave: TMDatasource
    DataSet = datasetSlave
  end
  object datasetMaster: TAureliusDataset
    FieldDefs = <
      item
        Name = 'Self'
        Attributes = [faReadonly]
        DataType = ftVariant
      end
      item
        Name = 'Id'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'name'
        Attributes = [faRequired]
        DataType = ftWideString
        Size = 255
      end>
    Left = 200
    Top = 152
    DesignClass = 'utables.TMUnit'
    object datasetMastername: TWideStringField
      DisplayLabel = 'UNITE'
      DisplayWidth = 20
      FieldName = 'name'
      Required = True
      Size = 255
    end
  end
  object datasetSlave: TAureliusDataset
    FieldDefs = <
      item
        Name = 'Self'
        Attributes = [faReadonly]
        DataType = ftVariant
      end
      item
        Name = 'ID'
        Attributes = [faReadonly, faRequired]
        DataType = ftInteger
      end
      item
        Name = 'Name'
        Attributes = [faRequired]
        DataType = ftWideString
        Size = 255
      end
      item
        Name = 'Parent'
        DataType = ftVariant
      end>
    Left = 88
    Top = 240
    DesignClass = 'utables.TMCat'
    object WideStringField2: TWideStringField
      DisplayLabel = 'CATEGUORIE'
      DisplayWidth = 50
      FieldName = 'Name'
      Required = True
      Size = 255
    end
    object AureliusEntityField3: TAureliusEntityField
      FieldName = 'Self'
      ReadOnly = True
    end
    object AureliusEntityField4: TAureliusEntityField
      FieldName = 'Parent'
    end
    object WideStringField3: TWideStringField
      DisplayLabel = 'PARENT CAT'
      FieldKind = fkLookup
      FieldName = 'parentName'
      LookupKeyFields = 'Self'
      LookupResultField = 'name'
      KeyFields = 'Parent'
      Size = 50
      Lookup = True
    end
  end
end
