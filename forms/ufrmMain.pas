unit ufrmMain;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.ComCtrls,
  Vcl.StdCtrls,
  System.ImageList,
  Vcl.ImgList,
  SVGIconImageListBase,
  SVGIconImageList,
  AdvPageControl,
  uMNPageControle,
  ufrmCon,
  uFramePageCtrl,
  Vcl.Menus,
  Vcl.ExtCtrls,
  Vcl.ToolWin,
  Vcl.Mask,
  Vcl.DBCtrls,
  AdvEdit,
  advlued,
  DBAdvLe,
  AdvToolBtn,
  Vcl.Buttons;

type
  TfrmMain = class(TForm)
    framePageCtrl: TframePageCtrl;
    MainMenu1: TMainMenu;
    ALES1: TMenuItem;
    ALES2: TMenuItem;
    ControlBar1: TControlBar;
    edit: TDBAdvLUEdit;
    ToolBar2: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolBar3: TToolBar;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button4Click(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
    procedure ToolButton11Click(Sender: TObject);
  private
    function IsShortCut(var message: TWMKey): Boolean; overload;
    procedure ActiveControlChanged(Sender: TObject);

    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

uses
  uConnection,
  uDbManager,
  utables,
  udbControls,uInterfaces;

procedure TfrmMain.ActiveControlChanged(Sender: TObject);
begin
  if System.SysUtils.Supports(Screen.ActiveControl, IEditable) then
    Self.framePageCtrl.activeFrameMaster.activeControl := Screen.ActiveControl
      as IEditable;

end;

procedure TfrmMain.Button4Click(Sender: TObject);
begin

//  var con := TMConnection.Create(Self, 'mysql', 'localhost', '3307', 'root', 'nooo', '');
//
//  con.Active := True;
//  con.CreateDb('mimi');
//  con := TMConnection.Create(Self, 'mysql', 'localhost', '3307', 'root', 'nooo', 'mimi');
//  con.Active := True;
//  var dbMan := TMDBManager.Create(Self, con, [TMDatabase, TMUser, TMUserGroup]);
//  dbMan.updateDatabase;
//  var d := TMDatabase.Create;
//  d.title := 'grp';
//  dbMan.save(d);
//  var grp := TMUserGroup.Create();
//  grp.name := 'hi';
//  var usr := TMUser.Create('hi', 'nono', '123', grp);
//  dbMan.save(usr);
end;

procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Application.Terminate;
end;


function TfrmMain.IsShortCut(var Message: TWMKey): Boolean;

  function DispatchShortCut(const Owner: TComponent): Boolean;
  begin
    Result := False;
    { Dispatch to all children }
    if frmMain.framePageCtrl.pageCtrl.PageCount = 0 then
    begin
      Result := False;
    end
    else
    begin
      var actList := frmMain.framePageCtrl.ActionList1;
      if actList.IsShortCut(Message) then
      begin
        Result := True;
        Exit;
      end;
    end;
  end;

begin
  Result := DispatchShortCut(Self);
  if not Result then
    Result := inherited;
end;

procedure TfrmMain.ToolButton11Click(Sender: TObject);
begin
  framePageCtrl.actUnitOpenExecute(Sender);

end;

procedure TfrmMain.ToolButton7Click(Sender: TObject);
begin
  framePageCtrl.actCatOpenExecute(Sender);

end;

end.

