object frmLogin: TfrmLogin
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'LOGIN'
  ClientHeight = 161
  ClientWidth = 404
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  Position = poDesktopCenter
  TextHeight = 15
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 404
    Height = 113
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 64
      Top = 27
      Width = 23
      Height = 15
      Caption = 'User'
    end
    object Label2: TLabel
      Left = 64
      Top = 56
      Width = 61
      Height = 15
      Caption = 'PASSWORD'
    end
    object txtUser: TEdit
      Left = 139
      Top = 24
      Width = 198
      Height = 23
      TabOrder = 0
      Text = 'admin'
      OnKeyPress = txtUserKeyPress
    end
    object txtPasss: TEdit
      Left = 139
      Top = 53
      Width = 121
      Height = 23
      PasswordChar = '*'
      TabOrder = 1
      Text = '1234'
      OnKeyPress = txtPasssKeyPress
    end
  end
  object btnLogin: TButton
    Left = 321
    Top = 128
    Width = 75
    Height = 25
    Caption = 'LOGIN'
    TabOrder = 1
    OnClick = btnLoginClick
  end
  object Button1: TButton
    Left = 240
    Top = 128
    Width = 75
    Height = 25
    Caption = 'CANCEL'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button3: TButton
    Left = 89
    Top = 128
    Width = 75
    Height = 25
    Caption = 'INIT'
    TabOrder = 3
    OnClick = Button3Click
  end
  object Button2: TButton
    Left = 8
    Top = 128
    Width = 75
    Height = 25
    Caption = 'DATABASES'
    TabOrder = 4
  end
  object Button4: TButton
    Left = 170
    Top = 128
    Width = 39
    Height = 25
    Caption = 'Button4'
    TabOrder = 5
    OnClick = Button4Click
  end
end
