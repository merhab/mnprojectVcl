object frmBase: TfrmBase
  Left = 0
  Top = 0
  Caption = 'frmBase'
  ClientHeight = 441
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  TextHeight = 15
  object grpMaster: TMGroupBox
    Left = 0
    Top = 0
    Width = 624
    Height = 313
    Align = alTop
    Caption = 'GRP_MASTER'
    TabOrder = 0
  end
  object grpSlave: TMGroupBox
    Left = 0
    Top = 313
    Width = 624
    Height = 128
    Align = alClient
    Caption = 'GRP_SLAVE'
    TabOrder = 1
  end
  object dtsMaster: TMDatasource
    Left = 64
    Top = 80
  end
  object dtsSlave: TMDatasource
    Left = 64
    Top = 144
  end
end
