object frmCon: TfrmCon
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 282
  Width = 526
  object conServer: TUniConnection
    ProviderName = 'mysqL'
    Port = 3307
    Username = 'root'
    Server = 'localhost'
    Left = 64
    Top = 8
    EncryptedPassword = '91FF90FF90FF90FF'
  end
  object AurConServer: TAureliusConnection
    AdapterName = 'UniDac'
    AdaptedConnection = conServer
    SQLDialect = 'MySql'
    Left = 64
    Top = 64
  end
  object aurConLocal: TAureliusConnection
    AdapterName = 'UniDac'
    AdaptedConnection = conLocal
    SQLDialect = 'Sqlite'
    Left = 280
    Top = 96
  end
  object events: TAureliusModelEvents
    Left = 360
    Top = 160
  end
  object conLocal: TUniConnection
    ProviderName = 'sqlite'
    SpecificOptions.Strings = (
      'sqlite.Direct=True'
      'sqlite.ForceCreateDatabase=True'
      'sqlite.LockingMode=lmNormal'
      'sqlite.UseUnicode=True')
    Server = 'localhost'
    LoginPrompt = False
    Left = 280
    Top = 24
  end
  object unsql: TUniSQL
    Connection = conServer
    Left = 216
    Top = 176
  end
end
