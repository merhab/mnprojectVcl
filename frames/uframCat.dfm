inherited frameCat: TframeCat
  Width = 542
  ExplicitWidth = 542
  inherited pnlControls: TGroupBox
    Width = 542
    Height = 58
    ExplicitWidth = 542
    ExplicitHeight = 58
    object Label1: TLabel
      Left = 18
      Top = 27
      Width = 68
      Height = 15
      Caption = 'CATEGUORIE'
    end
    object lblParent: TLabel
      Left = 318
      Top = 27
      Width = 42
      Height = 15
      Caption = 'PARENT'
    end
    object edtName: TMDbEdit
      Left = 95
      Top = 24
      Width = 193
      Height = 23
      DataField = 'name'
      DataSource = datasourceMaster
      TabOrder = 0
    end
    object cmbLookupParent: TMDBLookupComboBox
      Left = 392
      Top = 24
      Width = 145
      Height = 23
      DataField = 'parentName'
      DataSource = datasourceMaster
      TabOrder = 1
    end
  end
  inherited pnlGrid: TGroupBox
    Top = 83
    Width = 542
    ExplicitTop = 83
    ExplicitWidth = 542
    inherited DBGrid1: TDBGrid
      Width = 538
      Columns = <
        item
          Expanded = False
          FieldName = 'Name'
          Width = 228
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'parentName'
          Width = 190
          Visible = True
        end>
    end
  end
  inherited pnlSearch: TPanel
    Width = 542
    ExplicitWidth = 542
  end
  object Button1: TButton [3]
    Left = 528
    Top = 352
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 4
    OnClick = Button1Click
  end
  object Edit1: TEdit [4]
    Left = 392
    Top = 352
    Width = 121
    Height = 23
    TabOrder = 3
    Text = 'Edit1'
  end
  inherited datasourceMaster: TDataSource
    Left = 200
    Top = 136
  end
  inherited dataSourceSlave: TDataSource
    DataSet = datasetSlave
    Left = 288
    Top = 136
  end
  object DataSetLookUp: TAureliusDataset
    FieldDefs = <
      item
        Name = 'Self'
        Attributes = [faReadonly]
        DataType = ftVariant
      end
      item
        Name = 'ID'
        Attributes = [faReadonly, faRequired]
        DataType = ftInteger
      end
      item
        Name = 'Name'
        Attributes = [faRequired]
        DataType = ftWideString
        Size = 255
      end
      item
        Name = 'Parent'
        DataType = ftVariant
      end>
    Left = 384
    Top = 272
    DesignClass = 'utables.TMCat'
    object WideStringField1: TWideStringField
      FieldName = 'name'
      Required = True
      Size = 255
    end
    object AureliusEntityField1: TAureliusEntityField
      FieldName = 'Parent'
    end
    object AureliusEntityField2: TAureliusEntityField
      FieldName = 'Self'
      ReadOnly = True
    end
    object DataSetLookUpID: TIntegerField
      FieldName = 'ID'
      ReadOnly = True
      Required = True
    end
  end
  object DataSourceLookUp: TDataSource
    DataSet = DataSetLookUp
    Left = 280
    Top = 296
  end
  object datasetMaster: TAureliusDataset
    FieldDefs = <
      item
        Name = 'Self'
        Attributes = [faReadonly]
        DataType = ftVariant
      end
      item
        Name = 'ID'
        Attributes = [faReadonly, faRequired]
        DataType = ftInteger
      end
      item
        Name = 'Name'
        Attributes = [faRequired]
        DataType = ftWideString
        Size = 255
      end
      item
        Name = 'Parent'
        DataType = ftVariant
      end>
    Left = 48
    Top = 144
    DesignClass = 'utables.TMCat'
    object datasetMasterName: TWideStringField
      DisplayLabel = 'CATEGUORIE'
      DisplayWidth = 50
      FieldName = 'Name'
      Required = True
      Size = 255
    end
    object datasetMasterSelf: TAureliusEntityField
      FieldName = 'Self'
      ReadOnly = True
    end
    object datasetMasterParent: TAureliusEntityField
      FieldName = 'Parent'
    end
    object datasetMasterparentName: TWideStringField
      DisplayLabel = 'PARENT CAT'
      FieldKind = fkLookup
      FieldName = 'parentName'
      LookupDataSet = DataSetLookUp
      LookupKeyFields = 'Self'
      LookupResultField = 'name'
      KeyFields = 'Parent'
      Size = 50
      Lookup = True
    end
  end
  object datasetSlave: TAureliusDataset
    FieldDefs = <
      item
        Name = 'Self'
        Attributes = [faReadonly]
        DataType = ftVariant
      end
      item
        Name = 'ID'
        Attributes = [faReadonly, faRequired]
        DataType = ftInteger
      end
      item
        Name = 'Name'
        Attributes = [faRequired]
        DataType = ftWideString
        Size = 255
      end
      item
        Name = 'Parent'
        DataType = ftVariant
      end>
    Left = 80
    Top = 264
    DesignClass = 'utables.TMCat'
    object WideStringField2: TWideStringField
      DisplayLabel = 'CATEGUORIE'
      DisplayWidth = 50
      FieldName = 'Name'
      Required = True
      Size = 255
    end
    object AureliusEntityField3: TAureliusEntityField
      FieldName = 'Self'
      ReadOnly = True
    end
    object AureliusEntityField4: TAureliusEntityField
      FieldName = 'Parent'
    end
    object WideStringField3: TWideStringField
      DisplayLabel = 'PARENT CAT'
      FieldKind = fkLookup
      FieldName = 'parentName'
      LookupDataSet = DataSetLookUp
      LookupKeyFields = 'Self'
      LookupResultField = 'name'
      KeyFields = 'Parent'
      Size = 50
      Lookup = True
    end
  end
end
