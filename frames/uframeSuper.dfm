object FrameSuper: TFrameSuper
  Left = 0
  Top = 0
  Width = 640
  Height = 480
  TabOrder = 0
  object pnlControls: TGroupBox
    Left = 0
    Top = 25
    Width = 640
    Height = 169
    Align = alTop
    Caption = 'pnlControls'
    TabOrder = 0
  end
  object pnlGrid: TGroupBox
    Left = 0
    Top = 194
    Width = 640
    Height = 169
    Align = alTop
    Caption = 'pnlControls'
    TabOrder = 1
    object DBGrid1: TDBGrid
      Left = 2
      Top = 17
      Width = 636
      Height = 150
      Align = alClient
      DataSource = datasourceMaster
      ReadOnly = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = 'Segoe UI'
      TitleFont.Style = []
    end
  end
  object pnlSearch: TPanel
    Left = 0
    Top = 0
    Width = 640
    Height = 25
    Align = alTop
    AutoSize = True
    BevelOuter = bvNone
    Ctl3D = True
    ParentCtl3D = False
    TabOrder = 2
    Visible = False
    StyleName = 'Windows'
    object Label2: TLabel
      Left = 0
      Top = 4
      Width = 62
      Height = 15
      Caption = 'SEARCH      '
    end
    object lblFields: TLabel
      Left = 344
      Top = 4
      Width = 76
      Height = 15
      Caption = 'SEARCH FIELD'
    end
    object cmbFields: TComboBox
      Left = 455
      Top = 0
      Width = 145
      Height = 23
      Style = csDropDownList
      TabOrder = 0
    end
    object edtSearch: TEdit
      Left = 88
      Top = 2
      Width = 209
      Height = 23
      TabOrder = 1
    end
  end
  object datasourceMaster: TDataSource
    OnStateChange = datasourceMasterStateChange
    Left = 96
    Top = 32
  end
  object dataSourceSlave: TDataSource
    AutoEdit = False
    OnStateChange = datasourceMasterStateChange
    Left = 208
    Top = 48
  end
end
