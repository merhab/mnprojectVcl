﻿unit uFramePageCtrl;

interface

uses
  winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.ComCtrls,
  AdvPageControl,
  uframeSuper,
  System.ImageList,
  Vcl.ImgList,
  uAdvPageSheetPlus,
  uFrameUnite,
  System.Actions,
  Vcl.ActnList,
  Aurelius.Bind.Dataset,
  Vcl.Menus,
  Data.DB,
  System.UITypes,
  System.StrUtils,
  ulocalizer,uFrmImages;

type
  TFrameSuperClasses = class of TFrameSuper;

  TframePageCtrl = class(TFrame)
    pageCtrl: TAdvPageControl;
    ActionList1: TActionList;
    actUnitOpen: TAction;
    actDbFirst: TAction;
    actDbNext: TAction;
    actDbPrior: TAction;
    actDbLast: TAction;
    actDbLock: TAction;
    actDbSave: TAction;
    actDbCancel: TAction;
    actDbSearch: TAction;
    actDbNew: TAction;
    actCatOpen: TAction;
    actPageClose: TAction;
    procedure actUnitOpenExecute(Sender: TObject);
    procedure actDbFirstExecute(Sender: TObject);
    procedure actDbNextExecute(Sender: TObject);
    procedure actDbPriorExecute(Sender: TObject);
    procedure actDbLastExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure actDbLockExecute(Sender: TObject);
    procedure actDbSaveExecute(Sender: TObject);
    procedure actDbCancelExecute(Sender: TObject);
    procedure actDbSearchExecute(Sender: TObject);
    procedure actDbNewExecute(Sender: TObject);
    procedure actCatOpenExecute(Sender: TObject);
    procedure actPageCloseExecute(Sender: TObject);
  private


    function getActivePAge: TAdvPageSheetPlus;
    procedure setActivePAge(const Value: TAdvPageSheetPlus);
    function getDataSetMaster: TDataset;
    function getActiveFrameSuper: TFrameSuper;
  public
    function addPage(Caption: WideString; imgInd: Integer; frameClass:
      TFrameSuperClasses): TAdvTabSheet;
    procedure onMasterChanged(Sender: TObject);
    property activePAge: TAdvPageSheetPlus read getActivePAge write setActivePAge;
    constructor Create(AOwner: TComponent); override;
    property dataSetMaster: TDataset read getDataSetMaster ;
    property activeFrameMaster: TFrameSuper read getActiveFrameSuper;
  end;

implementation

{$R *.dfm}

uses
  uframcat;

procedure TframePageCtrl.actCatOpenExecute(Sender: TObject);
begin
  addPage(actCatOpen.Caption, actCatOpen.ImageIndex, TframeCat);

end;

procedure TframePageCtrl.actDbCancelExecute(Sender: TObject);
begin
  if pageCtrl.PageCount = 0 then
    Exit;

  if ((Self.activePAge.frame.datasourceSlave.State = dsEdit) or (Self.activePAge.frame.datasourceSlave.State
    = dsInsert)) then
  begin
    if activeFrameMaster.ReadOnly then
    begin
      ShowMessage(TR('PAGE IS READ ONLY'));
      Exit;
    end;
    activePAge.frame.datasourceSlave.DataSet.Cancel;
  end;

  if ((Self.activePAge.frame.datasourceMaster.State = dsEdit) or (Self.activePAge.frame.datasourceMaster.State
    = dsInsert)) then
  begin
    if activeFrameMaster.ReadOnly then
    begin
      ShowMessage(TR('PAGE IS READ ONLY'));
      Exit;
    end;
    activePAge.frame.datasourceMaster.DataSet.Cancel;
  end;

end;

procedure TframePageCtrl.actDbFirstExecute(Sender: TObject);
begin
  if pageCtrl.PageCount = 0 then
    Exit;
  if dataSetMaster.active then
    dataSetMaster.First;
end;

procedure TframePageCtrl.actDbLastExecute(Sender: TObject);
begin
  if pageCtrl.PageCount = 0 then
    Exit;
  if dataSetMaster.active then
    dataSetMaster.Last;
end;

procedure TframePageCtrl.actDbLockExecute(Sender: TObject);
begin
  if pageCtrl.PageCount = 0 then
    Exit;
  //***********
  Self.getActiveFrameSuper.readOnly := not Self.getActiveFrameSuper.readOnly;
  if activePAge.frame.readOnly then
  begin
    if not ContainsText(activePAge.stateCaption, '/🔏') then
      activePAge.stateCaption := activePAge.stateCaption + '/🔏';

  end
  else
  begin
    activePAge.stateCaption := StringReplace(activePAge.stateCaption, '/🔏', '',
      [rfReplaceAll]);
  end;
  TAdvTabSheet(activePAge).Caption := activePAge.caption + activePAge.stateCaption;
end;

procedure TframePageCtrl.actDbNewExecute(Sender: TObject);
begin
  if Self.activeFrameMaster.ReadOnly then
  begin
    ShowMessage(TR('PAGE IS READE ONLY'));
    Exit;
  end;
  if pageCtrl.PageCount = 0 then
    Exit;
  Self.activeFrameMaster.DatasetActive.Append;
end;

procedure TframePageCtrl.actDbNextExecute(Sender: TObject);
begin
  if pageCtrl.PageCount = 0 then
    Exit;
  if dataSetMaster.active then
    dataSetMaster.Next;
end;

procedure TframePageCtrl.actDbPriorExecute(Sender: TObject);
begin
  if pageCtrl.PageCount = 0 then
    Exit;
  if dataSetMaster.active then
    dataSetMaster.Prior;
end;

procedure TframePageCtrl.actDbSaveExecute(Sender: TObject);
begin
  if pageCtrl.PageCount = 0 then
    Exit;
  if ((Self.activePAge.frame.datasourceMaster.State = dsEdit) or (Self.activePAge.frame.datasourceMaster.State
    = dsInsert)) then
    activePAge.frame.datasourceMaster.DataSet.Post;
  if ((Self.activePAge.frame.datasourceSlave.State = dsEdit) or (Self.activePAge.frame.datasourceSlave.State
    = dsInsert)) then
    activePAge.frame.datasourceSlave.DataSet.Post;

end;

procedure TframePageCtrl.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
begin

  actDbNext.Enabled := (Self.pageCtrl.PageCount > 0) and (not dataSetMaster.eof);
  actDbPrior.Enabled := (Self.pageCtrl.PageCount > 0) and (not dataSetMaster.Bof);
  actDbLast.Enabled := (Self.pageCtrl.PageCount > 0) and (not dataSetMaster.Eof);
  actDbFirst.Enabled := (Self.pageCtrl.PageCount > 0) and (not dataSetMaster.Bof);
  actDbNew.Enabled := Self.pageCtrl.PageCount > 0;
  actDbSearch.Enabled := Self.pageCtrl.PageCount > 0;
  actDbCancel.Enabled := Self.pageCtrl.PageCount > 0;
  actDbLock.Enabled := Self.pageCtrl.PageCount > 0;
  actDbSave.Enabled := Self.pageCtrl.PageCount > 0;
  actPageClose.Enabled := Self.pageCtrl.PageCount > 0;
  Handled := True;
end;

procedure TframePageCtrl.actDbSearchExecute(Sender: TObject);
begin
  if pageCtrl.PageCount = 0 then
    Exit;
  Self.activeFrameMaster.pnlSearch.Visible := not Self.activeFrameMaster.pnlSearch.Visible;
  if not Self.activeFrameMaster.pnlSearch.Visible then
  begin
    Self.activeFrameMaster.DatasetActive.Filtered := False;
    Self.activeFrameMaster.edtSearch.Text := '';
  end
  else
  begin
    Self.activeFrameMaster.edtSearch.SetFocus;
  end;
end;

procedure TframePageCtrl.actPageCloseExecute(Sender: TObject);
begin
  if pageCtrl.PageCount = 0 then
    Exit;
  if TAdvPageSheetPlus(pageCtrl.ActivePage).isChanged then
  begin
    ShowMessage(TR('SAVE OR CANCEL CHANGES BEFORE CLOSE'));
    Exit;
  end;
  pageCtrl.CloseOpenedTab(pageCtrl.ActivePage);
end;

procedure TframePageCtrl.actUnitOpenExecute(Sender: TObject);
begin
  addPage(actUnitOpen.Caption, actUnitOpen.ImageIndex, TframeUnite);
end;

function TframePageCtrl.addPage(caption: WideString; imgInd: Integer; frameClass:
  TFrameSuperClasses): TAdvTabSheet;
begin
  var page := TAdvPageSheetPlus.Create(Self.pageCtrl, frameClass);
  page.Caption := TR(caption);
  page.ImageIndex := imgInd;
  page.Caption := caption;
  page.AdvPageControl := Self.pageCtrl;
  Self.pageCtrl.activePage := page;
  page.frame.OnStateChange := Self.onMasterChanged;
  actDbLock.Execute;
  page.frame.pnlSearch.Parent := page;
  page.frame.getFirstFocusedControl.setFocused;
  page.ShowClose := True;

  Result := page;
end;

constructor TframePageCtrl.Create(AOwner: TComponent);
begin
  inherited;
  actDbNext.ShortCut := ShortCut(VK_DOWN, []);
  actDbPrior.ShortCut := ShortCut(VK_UP, []);
  actDbFirst.ShortCut := ShortCut(VK_UP, [ssCtrl, ssAlt]);
  actDbLast.ShortCut := ShortCut(VK_DOWN, [ssCtrl, ssAlt]);
  actDbLock.ShortCut := ShortCut(vkL, [ssCtrl]);
  actDbSave.ShortCut := ShortCut(vkS, [ssCtrl]);
  actDbCancel.ShortCut := ShortCut(VK_ESCAPE, []);
  actDbSearch.ShortCut := ShortCut(vkF, [ssCtrl]);
  actDbNew.ShortCut := ShortCut(vkN, [ssCtrl]);
  actPageClose.ShortCut := ShortCut(vkQ, [ssCtrl]);
end;

function TframePageCtrl.getActiveFrameSuper: TFrameSuper;
begin
  Result := activePAge.frame;
end;

function TframePageCtrl.getActivePAge: TAdvPageSheetPlus;
begin
  Result := TAdvPageSheetPlus(Self.pageCtrl.ActivePage);
end;

function TframePageCtrl.getDataSetMaster: TDataset;
begin
  Result := activePAge.frame.datasetMaster;
end;

procedure TframePageCtrl.onMasterChanged(sender: TObject);
begin
  var changed :=((Self.activePAge.frame.datasourceMaster.State = dsEdit) or (Self.activePAge.frame.datasourceMaster.State
    = dsInsert)) or ((Self.activePAge.frame.datasourceSlave.State = dsEdit) or (Self.activePAge.frame.datasourceSlave.State
    = dsInsert));
  if activePAge.ischanged = changed then
    Exit
  else
    activePAge.ischanged := changed;

  if activePAge.ischanged then
  begin
    if not ContainsText(activePAge.stateCaption, '....✏️') then
      activePAge.stateCaption := activePAge.stateCaption + '....✏️'
  end
  else
  begin
    activePAge.stateCaption := StringReplace(activePAge.stateCaption, '....✏️',
      '', [rfReplaceAll]);
  end;
  TAdvTabSheet(activePAge).Caption := activePAge.caption + activePAge.stateCaption;
end;

procedure TframePageCtrl.setActivePAge(const Value: TAdvPageSheetPlus);
begin
  Self.pageCtrl.ActivePage := Value;
end;


end.

