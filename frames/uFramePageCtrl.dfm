object framePageCtrl: TframePageCtrl
  Left = 0
  Top = 0
  Width = 640
  Height = 480
  TabOrder = 0
  object pageCtrl: TAdvPageControl
    Left = 0
    Top = 0
    Width = 640
    Height = 480
    ActiveFont.Charset = DEFAULT_CHARSET
    ActiveFont.Color = clWindowText
    ActiveFont.Height = -11
    ActiveFont.Name = 'Segoe UI'
    ActiveFont.Style = []
    Align = alClient
    DoubleBuffered = True
    ParentShowHint = False
    ShowHint = True
    TabBackGroundColor = clBtnFace
    TabMargin.RightMargin = 0
    TabOverlap = 0
    TabStyle = tsDotNet
    Version = '2.0.4.2'
    PersistPagesState.Location = plRegistry
    PersistPagesState.Enabled = False
    TabOrder = 0
  end
  object ActionList1: TActionList
    Images = frmImages.SVGIconImageList1
    OnUpdate = ActionList1Update
    Left = 476
    Top = 202
    object actUnitOpen: TAction
      Category = 'TABLES'
      Caption = 'UNIT'
      ImageIndex = 17
      ImageName = 'measure-technical-drawing-svgrepo-com'
      OnExecute = actUnitOpenExecute
    end
    object actDbFirst: TAction
      Category = 'DBNAV'
      Caption = 'FIRST'
      ImageIndex = 6
      ImageName = 'first'
      OnExecute = actDbFirstExecute
    end
    object actDbNext: TAction
      Category = 'DBNAV'
      Caption = 'NEXT'
      ImageIndex = 10
      ImageName = 'play-svgrepo-com'
      OnExecute = actDbNextExecute
    end
    object actDbPrior: TAction
      Category = 'DBNAV'
      Caption = 'PRIOR'
      ImageIndex = 11
      ImageName = 'prior'
      OnExecute = actDbPriorExecute
    end
    object actDbLast: TAction
      Category = 'DBNAV'
      Caption = 'Last'
      ImageIndex = 5
      ImageName = 'end'
      OnExecute = actDbLastExecute
    end
    object actDbLock: TAction
      Category = 'DBNAV'
      Caption = 'LOCK'
      ImageIndex = 9
      ImageName = 'lock-svgrepo-com'
      OnExecute = actDbLockExecute
    end
    object actDbSave: TAction
      Category = 'DBNAV'
      Caption = 'SAVE'
      ImageIndex = 14
      ImageName = 'save-floppy-svgrepo-com'
      OnExecute = actDbSaveExecute
    end
    object actDbCancel: TAction
      Category = 'DBNAV'
      Caption = 'CANCEL'
      ImageIndex = 13
      ImageName = 'rewind-15-seconds-forward-svgrepo-com'
      OnExecute = actDbCancelExecute
    end
    object actDbSearch: TAction
      Category = 'DBNAV'
      Caption = 'SEARCH'
      ImageIndex = 15
      ImageName = 'search-alt-2-svgrepo-com'
      OnExecute = actDbSearchExecute
    end
    object actDbNew: TAction
      Category = 'DBNAV'
      Caption = 'NEW'
      ImageIndex = 1
      ImageName = 'add-svgrepo-com'
      OnExecute = actDbNewExecute
    end
    object actCatOpen: TAction
      Category = 'TABLES'
      Caption = 'CATEGUORIES'
      ImageIndex = 2
      ImageName = 'category-svgrepo-com'
      OnExecute = actCatOpenExecute
    end
    object actPageClose: TAction
      Category = 'TABS'
      Caption = 'CLOSE PAGE'
      Hint = 'CLOSE CURRENT PAGE'
      ImageIndex = 3
      ImageName = 'close-ellipse-svgrepo-com'
      OnExecute = actPageCloseExecute
    end
  end
end
