unit uFrameUnite;

interface

uses
  winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  uframeSuper,
  AdvDBFormLayouter,
  AdvDBFormPanel,
  Data.DB,
  Aurelius.Bind.BaseDataset,
  Aurelius.Bind.Dataset,
  Aurelius.Criteria.Base,
  ufrmCon,
  Aurelius.Engine.ObjectManager,
  Vcl.StdCtrls,
  Vcl.ComCtrls,
  Vcl.ToolWin,
  Vcl.Grids,
  Vcl.DBGrids,
  Vcl.Mask,
  Vcl.ExtCtrls,
  Vcl.Buttons,
  AdvEdit,
  advlued,
  udbControls,
  Vcl.DBCtrls,uInterfaces;

type
  TDbEdit = udbControls.TMDbEdit;

  TframeUnite = class(TFrameSuper)
    Label1: TLabel;
    edtName: TMDbEdit;
    datasetMaster: TAureliusDataset;
    datasetMastername: TWideStringField;
    datasetSlave: TAureliusDataset;
    WideStringField2: TWideStringField;
    AureliusEntityField3: TAureliusEntityField;
    AureliusEntityField4: TAureliusEntityField;
    WideStringField3: TWideStringField;

  private
    function getDataSetMaster: TDataset; override;
    function getDataSetSlave: TDataset; override;
  public
    var
      critMaster: tcriteria;
    procedure ConnectDataSetMaster; override;
    function getEditableControls: TEditableControls; override;
    function getFirstFocusedControl: IEditable; override;
    destructor Destroy; override;
  end;

var
  frameUnite: TframeUnite;

implementation

{$R *.dfm}

{ TFrameSuper1 }

uses
  uglobalLogin,
  udicDbServer,
  utables;

procedure TframeUnite.ConnectDataSetMaster;
begin
  var Manager := frmcon.createObjectManagerServer(Db.name);
  critMaster := Manager.Find<TMUnit>;
  datasetMaster.SetSourceCriteria(critMaster.Clone);
  datasetMaster.Manager := Manager;
  datasetMaster.Open;
  inherited;
end;

destructor TframeUnite.Destroy;
begin
  critMaster.free;
  datasetMaster.manager.free;
  inherited;
end;

function TframeUnite.getDataSetMaster: TDataset;
begin
  Result := Self.datasetMaster;
end;

function TframeUnite.getDataSetSlave: TDataset;
begin
  result := datasetSlave;
end;

function TframeUnite.getEditableControls: TEditableControls;
begin
  Result := [edtName];
end;

function TframeUnite.getFirstFocusedControl: IEditable;
begin
  result := edtName;
end;

end.

