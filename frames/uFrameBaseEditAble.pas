unit uFrameBaseEditAble;

interface

uses
  winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  uControlsInterfaces;

type
  TFrameBaseEditable = class(TFrame, IEditable)
  private
    { Private declarations }
  public
    procedure setReadOnly(val: boolean); virtual; abstract;
    procedure setText(const Value: WideString); virtual; abstract;
    function getText: WideString; virtual; abstract;
    function isFocused: Boolean; virtual; abstract;
    procedure setFocus; virtual; abstract;
    procedure editEnter(Sender: TObject);virtual; abstract;
  end;

implementation

{$R *.dfm}

end.

