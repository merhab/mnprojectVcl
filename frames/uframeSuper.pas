﻿unit uframeSuper;

interface

uses
  winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Data.DB,
  Vcl.StdCtrls,
  Vcl.ComCtrls,
  Vcl.ToolWin,
  Vcl.DBGrids,
  Vcl.ExtCtrls,
  Vcl.Buttons,
  udbControls,
  Vcl.DBCtrls,
  Vcl.Grids,
  System.StrUtils,uInterfaces;

type
  TFrameSuper = class(TFrame)
    datasourceMaster: TDataSource;
    pnlControls: TGroupBox;
    pnlGrid: TGroupBox;
    DBGrid1: TDBGrid;
    dataSourceSlave: TDataSource;
    pnlSearch: TPanel;
    Label2: TLabel;
    lblFields: TLabel;
    cmbFields: TComboBox;
    edtSearch: TEdit;
    procedure dataSetFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure datasourceMasterStateChange(Sender: TObject);
    function getReadOnly: Boolean;
    procedure edtSearchChange(Sender: TObject);
    procedure edtSearchEnter(Sender: TObject);
  private
    FOnStateChange: TNotifyEvent;
    FlastFocusedField: TField;
    FactiveControl: IEditable;
    function getDatasetActive: TDataSet;
    procedure actDbCancelExecute;
    procedure actDbFirstExecute;
    procedure actDbLastExecute;
    procedure actDbLockExecute;
    procedure actDbNewExecute;
    procedure actDbNextExecute;
    procedure actDbPriorExecute;
    procedure actDbSaveExecute;
    procedure actDbSearchExecute;
  protected
    function getDataSetMaster: TDataSet; virtual; abstract;
    function getDataSetSlave: TDataSet; virtual; abstract;
  public
    procedure ConnectdatasourceMaster; virtual; abstract;
    procedure ConnectDataSetMaster; virtual; abstract;
    function getEditableControls: TEditableControls; virtual; abstract;
    function getFirstFocusedControl: IEditable; virtual; abstract;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure setReadOnly(Val: Boolean);
    property readonly: Boolean read getReadOnly write setReadOnly;
    property OnStateChange: TNotifyEvent read FOnStateChange write FOnStateChange;
    property lastFocusedField: TField read FlastFocusedField write FlastFocusedField;
    property DatasetActive: TDataSet read getDatasetActive;
    property activeControl: IEditable read FactiveControl write FactiveControl;
    property dataSetMaster: TDataSet read getDataSetMaster;
    property dataSetSlave: TDataSet read getDataSetSlave;
    var
      searchWordList: TStringList;
  end;

implementation

{$R *.dfm}

{ TFrameSuper }

constructor TFrameSuper.Create(AOwner: TComponent);
begin
  inherited;
  searchWordList := TStringList.Create;
  dataSetMaster.OnFilterRecord := dataSetFilterRecord;
  dataSetSlave.OnFilterRecord := dataSetFilterRecord;
end;

procedure TFrameSuper.dataSetFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin

  Accept := True;
  var i: Integer;
  for i := 0 to searchWordList.Count - 1 do
  begin
    var j: Integer;
    var fldName: string;
    for j := 0 to DataSet.FieldCount - 1 do
      if DataSet.Fields[j].DisplayName = cmbFields.Text then
      begin
        fldName := DataSet.Fields[j].FieldName;
        Break;
      end;

    Accept := Accept and DataSet.FieldByName(fldName).AsString.ToUpper.Contains(searchWordList.Strings
      [i].ToUpper);
    if not Accept then
      Break;
  end;

end;

procedure TFrameSuper.datasourceMasterStateChange(Sender: TObject);
begin
  if Assigned(OnStateChange) then
    OnStateChange(Sender);
end;

destructor TFrameSuper.Destroy;
begin
  searchWordList.Free;
  inherited;
end;

procedure TFrameSuper.edtSearchChange(Sender: TObject);
begin
  DatasetActive.Filtered := False;
  searchWordList.Clear;
  searchWordList.Delimiter := '*';
  searchWordList.DelimitedText := edtSearch.Text;
  DatasetActive.filtered := True;
end;

procedure TFrameSuper.edtSearchEnter(Sender: TObject);
begin

  cmbFields.Items.Clear;
  var i: Integer;
  for i := 0 to DatasetActive.FieldCount - 1 do
  begin
    if DatasetActive.Fields[i].FieldKind = fkData then
    begin
      cmbFields.Items.Add(DatasetActive.Fields[i].DisplayName);
    end;
  end;
  if cmbFields.Text <> '' then
    Exit;
  if activeControl <> nil then
  begin
    cmbFields.Text := activeControl.GetField.DisplayName;
  end
  else
  begin
    cmbFields.ItemIndex := cmbFields.Items.IndexOf(getFirstFocusedControl.GetField.DisplayName);
//    cmbFields.Text := getFirstFocusedControl.GetField.DisplayName;
  end;

end;

function TFrameSuper.getDatasetActive: TDataSet;
begin
  if activeControl <> nil then
  begin
    Result := activeControl.getdataSource.DataSet;
  end
  else
  begin
    Result := getFirstFocusedControl.getdataSource.DataSet;
  end;
end;

function TFrameSuper.getReadOnly: Boolean;
begin
  Result := getFirstFocusedControl.getReadOnly;
end;

procedure TFrameSuper.setReadOnly(val: Boolean);
begin
  var i: Integer;
  for i := 0 to Self.ComponentCount - 1 do
    if Supports(Self.Components[i], IEditable) then
      (Self.Components[i] as IEditable).setReadOnly(val);
end;


//**********************************

procedure TFrameSuper.actDbCancelExecute;
begin

  if ((Self.datasourceSlave.dataSet.State = dsEdit) or (Self.datasourceSlave.dataSet.State
    = dsInsert)) then
  begin
    if Self.ReadOnly then
    begin
    //TODO:LOCALISATION
      ShowMessage('PAGE IS READ ONLY');
      Exit;
    end;
    dataSourceSlave.dataSet.Cancel;

  end;

  if ((datasourceMaster.dataSet.State = dsEdit) or (datasourceMaster.dataSet.State
    = dsInsert)) then
  begin
    if readonly then
    begin
    //TODO:LOCALISATION
      ShowMessage('PAGE IS READ ONLY');
      Exit;
    end;
    datasourceMaster.dataSet.Cancel;
  end;

end;

procedure TFrameSuper.actDbFirstExecute;
begin
  if DatasetActive.active then
    DatasetActive.First;
end;

procedure TFrameSuper.actDbLastExecute;
begin
  if DatasetActive.active then
    DatasetActive.Last;
end;

procedure TFrameSuper.actDbLockExecute;
begin
  readonly := not readonly;
end;

procedure TFrameSuper.actDbNewExecute;
begin
  if readonly then
  begin
  //TODO:LOCALIZATION
    ShowMessage('PAGE IS READE ONLY');
    Exit;
  end;
  DatasetActive.Append;
end;

procedure TFrameSuper.actDbNextExecute;
begin

  if DatasetActive.active then
    DatasetActive.Next;
end;

procedure TFrameSuper.actDbPriorExecute;
begin

  if DatasetActive.active then
    DatasetActive.Prior;
end;

procedure TFrameSuper.actDbSaveExecute;
begin

  if ((datasourceMaster.dataSet.State = dsEdit) or (datasourceMaster.dataSet.State
    = dsInsert)) then
    datasourceMaster.dataSet.Post;
  if ((dataSourceSlave.dataSet.State = dsEdit) or (dataSourceSlave.dataSet.State
    = dsInsert)) then
    dataSourceSlave.dataSet.Post;

end;

procedure TFrameSuper.actDbSearchExecute;
begin

  pnlSearch.Visible := not pnlSearch.Visible;
  if not pnlSearch.Visible then
  begin
    DatasetActive.Filtered := False;
    edtSearch.Text := '';
  end
  else
  begin
    edtSearch.SetFocus;
  end;
end;
{


procedure TframePageCtrl.actUnitOpenExecute(Sender: TObject);
begin
  addPage(actUnitOpen.Caption, actUnitOpen.ImageIndex, TframeUnite);
end;

function TframePageCtrl.addPage(caption: WideString; imgInd: Integer; frameClass:
  TFrameSuperClasses): TAdvTabSheet;
begin
  var page := TAdvPageSheetPlus.Create(Self.pageCtrl, frameClass);
  page.Caption := TR(caption);
  page.ImageIndex := imgInd;
  page.Caption := caption;
  page.AdvPageControl := Self.pageCtrl;
  Self.pageCtrl.activePage := page;
  page.frame.OnStateChange := Self.onMasterChanged;
  actDbLock.Execute;
  page.frame.pnlSearch.Parent := page;
  page.frame.getFirstFocusedControl.setFocused;
  page.ShowClose := True;

  Result := page;
end;

constructor TframePageCtrl.Create(AOwner: TComponent);
begin
  inherited;
  actDbNext.ShortCut := ShortCut(VK_DOWN, []);
  actDbPrior.ShortCut := ShortCut(VK_UP, []);
  actDbFirst.ShortCut := ShortCut(VK_UP, [ssCtrl, ssAlt]);
  actDbLast.ShortCut := ShortCut(VK_DOWN, [ssCtrl, ssAlt]);
  actDbLock.ShortCut := ShortCut(vkL, [ssCtrl]);
  actDbSave.ShortCut := ShortCut(vkS, [ssCtrl]);
  actDbCancel.ShortCut := ShortCut(VK_ESCAPE, []);
  actDbSearch.ShortCut := ShortCut(vkF, [ssCtrl]);
  actDbNew.ShortCut := ShortCut(vkN, [ssCtrl]);
  actPageClose.ShortCut := ShortCut(vkQ, [ssCtrl]);
end;

function TframePageCtrl.getActiveFrameSuper: TFrameSuper;
begin
  Result := frame;
end;

function TframePageCtrl.getActivePAge: TAdvPageSheetPlus;
begin
  Result := TAdvPageSheetPlus(Self.pageCtrl.ActivePage);
end;

function TframePageCtrl.getdatasourceMaster.dataSet: TAureliusDataset;
begin
  Result := frame.datasourceMaster.dataSet;
end;

procedure TframePageCtrl.onMasterChanged(sender: TObject);
begin
  var changed :=((datasourceMaster.dataSet.State = dsEdit) or (datasourceMaster.dataSet.State
    = dsInsert)) or ((datasourceSlave.dataSetState = dsEdit) or (datasourceSlave.dataSetState
    = dsInsert));
  if ischanged = changed then
    Exit
  else
    ischanged := changed;

  if ischanged then
  begin
    if not ContainsText(stateCaption, '....✏️') then
      stateCaption := stateCaption + '....✏️'
  end
  else
  begin
    stateCaption := StringReplace(stateCaption, '....✏️',
      '', [rfReplaceAll]);
  end;
  TAdvTabSheet(activePAge).Caption := caption + stateCaption;
end;

procedure TframePageCtrl.setActivePAge(const Value: TAdvPageSheetPlus);
begin
  Self.pageCtrl.ActivePage := Value;
end;

procedure TframePageCtrl.setdatasourceMaster.dataSet(const Value: TAureliusDataset);
begin
  frame.datasourceMaster.dataSet := Value;
end;

 }

end.

