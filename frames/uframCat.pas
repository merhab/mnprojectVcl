unit uframCat;

interface

uses
  winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  uframeSuper,
  Data.DB,
  Aurelius.Bind.BaseDataset,
  Aurelius.Bind.Dataset,
  Vcl.StdCtrls,
  Vcl.DBCtrls,
  udbControls,
  Vcl.Mask,
  Vcl.ExtCtrls,
  Vcl.Grids,
  Vcl.DBGrids,
  Aurelius.Criteria.Base,
  AdvEdit,
  advlued,
  Vcl.Buttons,
  uglobalLogin,uInterfaces;

type
  TframeCat = class(TFrameSuper)
    edtName: TMDbEdit;
    Label1: TLabel;
    DataSetLookUp: TAureliusDataset;
    WideStringField1: TWideStringField;
    AureliusEntityField1: TAureliusEntityField;
    AureliusEntityField2: TAureliusEntityField;
    DataSourceLookUp: TDataSource;
    Button1: TButton;
    Edit1: TEdit;
    cmbLookupParent: TMDBLookupComboBox;
    DataSetLookUpID: TIntegerField;
    lblParent: TLabel;
    datasetMaster: TAureliusDataset;
    datasetMasterName: TWideStringField;
    datasetMasterSelf: TAureliusEntityField;
    datasetMasterParent: TAureliusEntityField;
    datasetMasterparentName: TWideStringField;
    datasetSlave: TAureliusDataset;
    WideStringField2: TWideStringField;
    AureliusEntityField3: TAureliusEntityField;
    AureliusEntityField4: TAureliusEntityField;
    WideStringField3: TWideStringField;
    procedure Button1Click(Sender: TObject);
  private
    function getDataSetMaster: TDataSet; override;
    function getDataSetSlave: TDataSet; override;
  public
    { Public declarations }
    var
      critMaster: TCriteria;
    procedure ConnectDataSetMaster; override;
    function getEditableControls: TEditableControls; override;
    function getFirstFocusedControl: IEditable; override;
    destructor destroy; override;
  end;

var
  frameCat: TframeCat;

implementation

{$R *.dfm}

uses
  ufrmCon,
  utables;

{ TFrameSuper1 }

procedure TframeCat.Button1Click(Sender: TObject);
begin
  cmbLookupParent.setText(Edit1.Text);
end;

procedure TframeCat.ConnectDataSetMaster;
begin
  var Manager := frmCon.createObjectManagerServer(db.name);
  critMaster := Manager.Find<TMCat>;
  datasetMaster.SetSourceCriteria(critMaster.Clone);
  datasetMaster.Manager := Manager;
  DataSetLookUp.SetSourceCriteria(critMaster.Clone);
  DataSetLookUp.Open;
 // datasetMaster.SetSourceList(Manager.Find<TMCat>.list);
  datasetMaster.Open;
  inherited;
end;

destructor TframeCat.destroy;
begin
  critMaster.Free;
  dataSetMaster.Manager.Free;
  inherited;
end;

function TframeCat.getDataSetMaster: TDataSet;
begin
  Result := datasetMaster;
end;

function TframeCat.getDataSetSlave: TDataSet;
begin
  Result := dataSetSlave;
end;

function TframeCat.getEditableControls: TEditableControls;
begin
  Result := [edtName, cmbLookupParent];
end;

function TframeCat.getFirstFocusedControl: IEditable;
begin
  result := edtName;
end;

end.

