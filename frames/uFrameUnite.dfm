inherited frameUnite: TframeUnite
  Width = 304
  Height = 377
  AutoSize = True
  ExplicitWidth = 304
  ExplicitHeight = 377
  inherited pnlControls: TGroupBox
    Width = 304
    Height = 65
    Caption = ''
    ExplicitWidth = 304
    ExplicitHeight = 65
    object Label1: TLabel
      Left = 18
      Top = 27
      Width = 32
      Height = 15
      Caption = 'UNITE'
    end
    object edtName: TMDbEdit
      Left = 64
      Top = 24
      Width = 193
      Height = 23
      DataField = 'name'
      DataSource = datasourceMaster
      TabOrder = 0
    end
  end
  inherited pnlGrid: TGroupBox
    Top = 90
    Width = 304
    Height = 287
    Align = alClient
    Caption = 'UNITE LIST'
    ExplicitTop = 90
    ExplicitWidth = 304
    ExplicitHeight = 287
    inherited DBGrid1: TDBGrid
      Width = 300
      Height = 268
      Columns = <
        item
          Expanded = False
          FieldName = 'name'
          Visible = True
        end>
    end
  end
  inherited pnlSearch: TPanel
    Width = 304
    ExplicitWidth = 304
  end
  inherited datasourceMaster: TDataSource
    Left = 72
    Top = 128
  end
  inherited dataSourceSlave: TDataSource
    DataSet = datasetSlave
    Left = 184
    Top = 88
  end
  object datasetMaster: TAureliusDataset
    FieldDefs = <
      item
        Name = 'Self'
        Attributes = [faReadonly]
        DataType = ftVariant
      end
      item
        Name = 'Id'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'name'
        Attributes = [faRequired]
        DataType = ftWideString
        Size = 255
      end>
    Left = 192
    Top = 176
    DesignClass = 'utables.TMUnit'
    object datasetMastername: TWideStringField
      DisplayLabel = 'UNITE'
      DisplayWidth = 20
      FieldName = 'name'
      Required = True
      Size = 255
    end
  end
  object datasetSlave: TAureliusDataset
    FieldDefs = <
      item
        Name = 'Self'
        Attributes = [faReadonly]
        DataType = ftVariant
      end
      item
        Name = 'ID'
        Attributes = [faReadonly, faRequired]
        DataType = ftInteger
      end
      item
        Name = 'Name'
        Attributes = [faRequired]
        DataType = ftWideString
        Size = 255
      end
      item
        Name = 'Parent'
        DataType = ftVariant
      end>
    Left = 80
    Top = 264
    DesignClass = 'utables.TMCat'
    object WideStringField2: TWideStringField
      DisplayLabel = 'CATEGUORIE'
      DisplayWidth = 50
      FieldName = 'Name'
      Required = True
      Size = 255
    end
    object AureliusEntityField3: TAureliusEntityField
      FieldName = 'Self'
      ReadOnly = True
    end
    object AureliusEntityField4: TAureliusEntityField
      FieldName = 'Parent'
    end
    object WideStringField3: TWideStringField
      DisplayLabel = 'PARENT CAT'
      FieldKind = fkLookup
      FieldName = 'parentName'
      LookupKeyFields = 'Self'
      LookupResultField = 'name'
      KeyFields = 'Parent'
      Size = 50
      Lookup = True
    end
  end
end
