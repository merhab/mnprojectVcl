﻿unit uMPageControl;

interface

uses
  vcl.ComCtrls,
  ufrmBase,
  uDbMasterSlaveCore,
  System.Classes,
  Vcl.Controls,
  db,
  Vcl.Dialogs,
  System.StrUtils,
  System.SysUtils,
  Vcl.Forms,
  types,
  Vcl.Themes,
  math,
  Winapi.Windows,
  Winapi.Messages,
  Vcl.Graphics;

type
  TFrmBaseClass = class of TfrmBase;

  TMformSheet = class(TTabSheet)
  private
    Fform: TfrmBase;
    Fchanged: Boolean;
    FCaption: WideString;
    FstateCaption: WideString;
    function getCaption: WideString;
    function getStateCaption: WideString;
    procedure SetCaption(const Value: WideString);
    procedure setStateCaption(const Value: WideString);

  public
    constructor Create(AOwner: TComponent; claz: TFrmBaseClass); overload;
    property form: TfrmBase read Fform write Fform;
    property Changed: Boolean read Fchanged write Fchanged;
    property Caption: WideString read getCaption write SetCaption;
    property stateCaption: WideString read getStateCaption write setStateCaption;
    destructor Destroy; override;
  end;

  TMformPageControl = class(TPageControl)
  private
    FonformChane: TNotifyEvent;
    FCloseButtonsRect: array of TRect;
    FCloseButtonMouseDownIndex: Integer;
    FCloseButtonShowPushed: Boolean;
    FOnMouseLeave: TNotifyEvent;
    FOnStateChange: TNotifyEvent;
    FonLock: TLockNotifyEvent;
    FonBeforeCancel: TBeforeActionNotifyEvent;
    FonBeforePost: TBeforeActionNotifyEvent;
    FonBeforeMove: TBeforeActionNotifyEvent;
    FonBeforeDelete: TBeforeActionNotifyEvent;
    FonBeforeAppend: TBeforeActionNotifyEvent;
    FonAfterCancel: TNotifyEvent;
    FonAfterPost: TNotifyEvent;
    FonAfterMove: TNotifyEvent;
    FonAfterDelete: TNotifyEvent;
    FonAfterAppend: TNotifyEvent;
    FonFilterRecord: TFilterNotifyEvent;
    FonAfterFilterREcord: TNotifyEvent;
    FonBeforeFilterRecord: TFilterNotifyEvent;
    function addPage(Caption: WideString; imgInd: Integer; formClass:
      TFrmBaseClass): TMformSheet;
    procedure actPageCloseExecute;
    procedure DrawMyTab(Control: TCustomTabControl; TabIndex: Integer; const
      Rect: TRect; Active: Boolean);
    procedure DrawTab(TabIndex: Integer; const Rect: TRect; Active: Boolean); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
      override;
    procedure MyMouseDown(Sender: TObject; Button: TMouseButton; Shift:
      TShiftState; X, Y: Integer);
    procedure myMouseLeave(Sender: TObject);
    procedure myMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure myMouseUp(Sender: TObject; Button: TMouseButton; Shift:
      TShiftState; X, Y: Integer);
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure CMMouseLeave(var message: TMessage); message CM_MOUSEENTER;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure setOnStateChange(const Value: TNotifyEvent);

  public
    function getActivePage: TMformSheet;
    constructor Create(AOwner: TComponent); override;
  published
    property onformChange: TNotifyEvent read FonformChane write FonformChane;
    property OnMouseLeave: TNotifyEvent read FOnMouseLeave write FOnMouseLeave;
    property OnStateChange: TNotifyEvent read FOnStateChange write setOnStateChange;
    property onLock: TLockNotifyEvent read FonLock write FonLock;
    property onBeforeMove: TBeforeActionNotifyEvent read FonBeforeMove write
      FonBeforeMove;
    property onAfterMove: TNotifyEvent read FonAfterMove write FonAfterMove;
    property onBeforeAppend: TBeforeActionNotifyEvent read FonBeforeAppend write
      FonBeforeAppend;
    property onAfterAppend: TNotifyEvent read FonAfterAppend write FonAfterAppend;
    property onBeforePost: TBeforeActionNotifyEvent read FonBeforePost write
      FonBeforePost;
    property onAfterPost: TNotifyEvent read FonAfterPost write FonAfterPost;
    property onBeforeCancel: TBeforeActionNotifyEvent read FonBeforeCancel write
      FonBeforeCancel;
    property onBeforeDelete: TBeforeActionNotifyEvent read FonBeforeDelete write
      FonBeforeDelete;
    property onAfterDelete: TNotifyEvent read FonAfterDelete write FonAfterDelete;

    property onAfterCancel: TNotifyEvent read FonAfterCancel write FonAfterCancel;
    property onBeforeFilterRecord: TFilterNotifyEvent read FonBeforeFilterRecord
      write FonBeforeFilterRecord;
    property onAfterFilterREcord: TNotifyEvent read FonAfterFilterREcord write
      FonAfterFilterREcord;
    property onFilterRecord: TFilterNotifyEvent read FonFilterRecord write
      FonFilterRecord;
  end;

implementation

{ TMPageControl }

function TMformPageControl.getActivePage: TMformSheet;
begin
  Result := TMformSheet(ActivePage);
end;

function TMformPageControl.addPage(caption: WideString; imgInd: Integer;
  formClass: TFrmBaseClass): TMformSheet;
begin
  var page := TMformSheet.Create(Self, formClass);
  page.Caption := caption;
  page.ImageIndex := imgInd;
  page.Caption := caption;

  Self.activePage := page;
  if Assigned(onformChange) then
    page.form.Manager.OnStateChange := Self.onformChange;
  page.form.manager.lock(True);
  //page.ShowClose := True;

  Result := page;
end;

procedure TMformPageControl.DrawTab(TabIndex: Integer; const Rect: TRect; Active:
  Boolean);
var
  CloseBtnSize: Integer;
  PageControl: TPageControl;
  TabCaption: TPoint;
  CloseBtnRect: TRect;
  CloseBtnDrawState: Cardinal;
  CloseBtnDrawDetails: TThemedElementDetails;
const
  UseThemes: Boolean = True;
begin
  PageControl := Self;

  if InRange(TabIndex, 0, Length(FCloseButtonsRect) - 1) then
  begin
    CloseBtnSize := 14;
    TabCaption.Y := Rect.Top + 3;

    if Active then
    begin
      CloseBtnRect.Top := Rect.Top + 4;
      CloseBtnRect.Right := Rect.Right - 5;
      TabCaption.X := Rect.Left + 6;
    end
    else
    begin
      CloseBtnRect.Top := Rect.Top + 3;
      CloseBtnRect.Right := Rect.Right - 5;
      TabCaption.X := Rect.Left + 3;
    end;
    var LImageWidth: Integer;
    var LImageHeight: Integer;
    var LImageSpacing := 10;
    if (Images <> nil) and (ActivePage.ImageIndex >= 0) and (ActivePage.ImageIndex
      < Images.Count) then
    begin
      LImageWidth := Images.Width;
      LImageHeight := Images.Height;

    end
    else
    begin
      LImageWidth := 0;
      LImageHeight := 0;
      LImageSpacing := 0;
    end;
    CloseBtnRect.Bottom := CloseBtnRect.Top + CloseBtnSize;
    CloseBtnRect.Left := CloseBtnRect.Right - CloseBtnSize;
    FCloseButtonsRect[TabIndex] := CloseBtnRect;

    PageControl.Canvas.FillRect(Rect);
    PageControl.Canvas.TextOut(TabCaption.X + 20, TabCaption.Y, PageControl.Pages
      [TabIndex].caption);

    var Icon := TIcon.Create;
    try
      with PageControl.Canvas do
      begin
        Pen.Color := clBlue;
        Self.Images.GetIcon(PageControl.Pages[TabIndex].ImageIndex, Icon);
        Draw(TabCaption.X, TabCaption.Y, Icon);
      end;
    finally
      Icon.free;
    end;
    if not UseThemes then
    begin
      if (FCloseButtonMouseDownIndex = TabIndex) and FCloseButtonShowPushed then
        CloseBtnDrawState := DFCS_CAPTIONCLOSE + DFCS_PUSHED
      else
        CloseBtnDrawState := DFCS_CAPTIONCLOSE;

      DrawFrameControl(PageControl.Canvas.Handle, FCloseButtonsRect[TabIndex],
        DFC_CAPTION, CloseBtnDrawState);
    end
    else
    begin
      Dec(FCloseButtonsRect[TabIndex].Left);

      if (FCloseButtonMouseDownIndex = TabIndex) and FCloseButtonShowPushed then
        CloseBtnDrawDetails := ThemeServices.GetElementDetails(twCloseButtonPushed)
      else
        CloseBtnDrawDetails := ThemeServices.GetElementDetails(twCloseButtonNormal);

      ThemeServices.DrawElement(PageControl.Canvas.Handle, CloseBtnDrawDetails,
        FCloseButtonsRect[TabIndex]);
    end;

  end;

  //Self.DrawMyTab(Self, TabIndex, Rect, Active);
    //inherited drawTab(tabindex,Rect,active);
end;

procedure TMformPageControl.actPageCloseExecute;
begin
  if PageCount = 0 then
    Exit;
  if getActivePage.Changed then
  begin
  //TODO:LOCALIZATION
    ShowMessage('SAVE OR CANCEL CHANGES BEFORE CLOSE');
    Exit;
  end;
  //ActivePage.close;
end;

procedure TMformPageControl.DrawMyTab(Control: TCustomTabControl; TabIndex:
  Integer; const Rect: TRect; Active: Boolean);
var
  CloseBtnSize: Integer;
  PageControl: TPageControl;
  TabCaption: TPoint;
  CloseBtnRect: TRect;
  CloseBtnDrawState: Cardinal;
  CloseBtnDrawDetails: TThemedElementDetails;
const
  UseThemes: Boolean = True;
begin
  PageControl := Control as TPageControl;

  if InRange(TabIndex, 0, Length(FCloseButtonsRect) - 1) then
  begin
    CloseBtnSize := 14;
    TabCaption.Y := Rect.Top + 3;

    if Active then
    begin
      CloseBtnRect.Top := Rect.Top + 4;
      CloseBtnRect.Right := Rect.Right - 5;
      TabCaption.X := Rect.Left + 6;
    end
    else
    begin
      CloseBtnRect.Top := Rect.Top + 3;
      CloseBtnRect.Right := Rect.Right - 5;
      TabCaption.X := Rect.Left + 3;
    end;
    var LImageWidth: Integer;
    var LImageHeight: Integer;
    var LImageSpacing := 10;
    if (Images <> nil) and (ActivePage.ImageIndex >= 0) and (ActivePage.ImageIndex
      < Images.Count) then
    begin
      LImageWidth := Images.Width;
      LImageHeight := Images.Height;

    end
    else
    begin
      LImageWidth := 0;
      LImageHeight := 0;
      LImageSpacing := 0;
    end;
    CloseBtnRect.Bottom := CloseBtnRect.Top + CloseBtnSize;
    CloseBtnRect.Left := CloseBtnRect.Right - CloseBtnSize;
    FCloseButtonsRect[TabIndex] := CloseBtnRect;

    PageControl.Canvas.FillRect(Rect);
    PageControl.Canvas.TextOut(TabCaption.X, TabCaption.Y, PageControl.Pages[TabIndex].caption);

    if not UseThemes then
    begin
      if (FCloseButtonMouseDownIndex = TabIndex) and FCloseButtonShowPushed then
        CloseBtnDrawState := DFCS_CAPTIONCLOSE + DFCS_PUSHED
      else
        CloseBtnDrawState := DFCS_CAPTIONCLOSE;

      DrawFrameControl(PageControl.Canvas.Handle, FCloseButtonsRect[TabIndex],
        DFC_CAPTION, CloseBtnDrawState);
    end
    else
    begin
      Dec(FCloseButtonsRect[TabIndex].Left);

      if (FCloseButtonMouseDownIndex = TabIndex) and FCloseButtonShowPushed then
        CloseBtnDrawDetails := ThemeServices.GetElementDetails(twCloseButtonPushed)
      else
        CloseBtnDrawDetails := ThemeServices.GetElementDetails(twCloseButtonNormal);

      ThemeServices.DrawElement(PageControl.Canvas.Handle, CloseBtnDrawDetails,
        FCloseButtonsRect[TabIndex]);
    end;

  end;
end;
//

procedure TMformPageControl.MouseDown(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  inherited;
  MyMouseDown(Self, Button, Shift, X, Y);
end;
//

procedure TMformPageControl.MyMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  I: Integer;
begin
  if Button = mbLeft then
  begin
    for I := 0 to Length(FCloseButtonsRect) - 1 do
    begin
      if PtInRect(FCloseButtonsRect[I], Point(X, Y)) then
      begin
        FCloseButtonMouseDownIndex := I;
        FCloseButtonShowPushed := True;
        Repaint;
      end;
    end;
  end;
end;
//

procedure TMformPageControl.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  myMouseMove(Self, Shift, X, Y);
end;
//

procedure TMformPageControl.myMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
  Inside: Boolean;
begin
  if (ssLeft in Shift) and (FCloseButtonMouseDownIndex >= 0) then
  begin
    Inside := PtInRect(FCloseButtonsRect[FCloseButtonMouseDownIndex], Point(X, Y));

    if FCloseButtonShowPushed <> Inside then
    begin
      FCloseButtonShowPushed := Inside;
      Repaint;
    end;
  end;
end;
//

procedure TMformPageControl.CMMouseLeave(var Message: TMessage);
begin
  if Parent <> nil then
{$IF DEFINED(CLR)}
    DoMouseLeave;
  if (Message.WParam = 0) then
{$ELSE}
    Parent.Perform(CM_MOUSELEAVE, 0, Winapi.Windows.LPARAM(Self));
  if (Message.LParam = 0) then
{$ENDIF}
  begin
    if Assigned(FOnMouseLeave) then
      FOnMouseLeave(Self);

    if Self.ShowHint and not (csDesigning in ComponentState) then
      if CustomHint <> nil then
        CustomHint.HideHint(Self);
  end;
  myMouseLeave(Self);
end;

constructor TMformPageControl.Create(AOwner: TComponent);
begin
  inherited;
//   TabWidth := 150;
//  OwnerDraw := True;
end;

//
procedure TMformPageControl.myMouseLeave(Sender: TObject);
var
  PageControl: TPageControl;
begin
  FCloseButtonShowPushed := False;
  Repaint;
end;
//

procedure TMformPageControl.MouseUp(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
  myMouseUp(Self, Button, Shift, X, Y);
end;
//

procedure TMformPageControl.myMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  PageControl: TPageControl;
begin

  if (Button = mbLeft) and (FCloseButtonMouseDownIndex >= 0) then
  begin
    if PtInRect(FCloseButtonsRect[FCloseButtonMouseDownIndex], Point(X, Y)) then
    begin
      var ind := ActivePageIndex;
      var pg := ActivePage;
      Pages[ActivePageIndex].TabVisible := False;
      if PageCount > 0 then
        if ind > 0 then
          ActivePageIndex := ind - 1
        else
          ActivePageIndex := 0;
      pg.Free;
      FCloseButtonMouseDownIndex := -1;
      Repaint;
    end;
  end;
end;

procedure TMformPageControl.setOnStateChange(const Value: TNotifyEvent);
begin
  FOnStateChange := Value;
end;

{ TMformSheet }

constructor TMformSheet.Create(AOwner: TComponent; claz: TFrmBaseClass);
begin
  inherited Create(AOwner);
  Self.form := claz.Create(Self);
  Self.form.name := 'form';
  Self.PageControl := TMformPageControl(AOwner);
  //**************************************************
    //should be done on every change of the page count
  SetLength(TMformPageControl(AOwner).FCloseButtonsRect, TMformPageControl(AOwner).PageCount);
  TMformPageControl(AOwner).FCloseButtonMouseDownIndex := -1;
  var i: Integer;
  for i := 0 to Length(TMformPageControl(AOwner).FCloseButtonsRect) - 1 do
  begin
    TMformPageControl(AOwner).FCloseButtonsRect[i] := Rect(0, 0, 0, 0);
  end;
  //****************************************
  with TMformPageControl(AOwner) do
  begin
    if Assigned(FOnStateChange) then
    begin
      form.Manager.OnStateChange := FOnStateChange;
    end;
    if Assigned(FonLock) then
    begin
      form.Manager.onLock := FonLock;
    end;
    if Assigned(FonBeforeMove) then
    begin
      form.Manager.onBeforeMove := FonBeforeMove;
    end;
    if Assigned(FonAfterMove) then
    begin
      form.Manager.onAfterMove := FonAfterMove;
    end;
    if Assigned(FonBeforePost) then
    begin
      form.Manager.onBeforePost := FonBeforePost;
    end;
    if Assigned(FonAfterPost) then
    begin
      form.Manager.onAfterPost := FonAfterPost;
    end;
    if Assigned(FonBeforeCancel) then
    begin
      form.Manager.onBeforeCancel := FonBeforeCancel;
    end;
    if Assigned(FonAfterCancel) then
    begin
      form.Manager.onAfterCancel := FonAfterCancel;
    end;
    if Assigned(FonBeforeDelete) then
    begin
      form.Manager.onBeforeDelete := FonBeforeDelete;
    end;
    if Assigned(FonAfterDelete) then
    begin
      form.Manager.onAfterDelete := FonAfterDelete;
    end;
    if Assigned(FonBeforeAppend) then
    begin
      form.Manager.onBeforeAppend := FonBeforeAppend;
    end;
    if Assigned(FonAfterAppend) then
    begin
      form.Manager.onAfterAppend := FonAfterAppend;
    end;
    if Assigned(FonFilterRecord) then
    begin
      form.Manager.onFilterRecord := FonFilterRecord;
    end;
    if Assigned(FonBeforeFilterRecord) then
    begin
      form.Manager.onBeforeFilterRecord := FonBeforeFilterRecord;
    end;
    if Assigned(FonAfterFilterREcord) then
    begin
      form.Manager.onAfterFilterREcord := FonAfterFilterREcord;
    end;
  end;
  form.ManualDock(Self, nil, alClient);
  form.Show;
  Self.Caption := form.Caption;
end;

destructor TMformSheet.Destroy;
begin
  form.Free;
  inherited;
end;

function TMformSheet.getCaption: WideString;
begin
  Result := FCaption;
end;

function TMformSheet.getStateCaption: WideString;
begin
  Result := FstateCaption;
end;

procedure TMformSheet.SetCaption(const Value: WideString);
begin
  FCaption := Value;
  inherited caption := FCaption + FstateCaption;
end;

procedure TMformSheet.setStateCaption(const Value: WideString);
begin
  FstateCaption := Value;
  inherited caption := FCaption + FstateCaption;
end;

end.

