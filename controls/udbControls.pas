unit udbControls;

interface

uses
  vcl.DBCtrls,System.SysUtils,
  db,uInterfaces;

type

  TMDbEdit = class(TDBEdit, IEditable,Ilockable)
    procedure setReadOnly(Val: Boolean);
    function getReadOnly: Boolean;
    procedure setText(const Value: WideString);
    function getText: WideString;
    function getFieldName: string;
    function getIsFocused: Boolean;
    procedure setFocused;
    function GetField: TField;
    function getDataSource: TDataSource;
  end;

  TMDbComboBox = class(TDBComboBox, IEditable,Ilockable)
    procedure setReadOnly(Val: Boolean);
    function getReadOnly: Boolean;
    procedure setText(const Value: WideString);
    function getText: WideString;
    function getFieldName: string;
    function getIsFocused: Boolean;
    procedure setFocused;
    function GetField: TField;
    function getDataSource: TDataSource;
  end;

  TMDBLookupComboBox = class(TDBLookupComboBox, IEditable,Ilockable)
    procedure setReadOnly(Val: Boolean);
    function getReadOnly: Boolean;
    procedure setText(const Value: WideString);
    function getText: WideString;
    function getFieldName: string;
    function getIsFocused: Boolean;
    procedure setFocused;
    function GetField: TField;
    function getDataSource: TDataSource;
  end;

  TEditableControls = array of IEditable;

implementation

{ TDbEdit }

function TMDbEdit.getDataSource: TDataSource;
begin
  Result := Self.DataSource;
end;

function TMDbEdit.GetField: TField;
begin
  Result := Self.DataSource.DataSet.FieldByName(getFieldName);
end;

function TMDbEdit.getFieldName: string;
begin
  Result := Self.datafield;
end;

function TMDbEdit.getIsFocused: Boolean;
begin
  Result := Self.Focused;
end;

function TMDbEdit.getReadOnly: Boolean;
begin
  Result := Self.ReadOnly;
end;

function TMDbEdit.getText: WideString;
begin
  Result := Self.Text;
end;

procedure TMDbEdit.setFocused;
begin
  Self.SetFocus;
end;

procedure TMDbEdit.setReadOnly(val: Boolean);
begin
  Self.ReadOnly := val;
end;

procedure TMDbEdit.setText(const Value: WideString);
begin
  Self.Text := Value
end;

{ TMDbComboBox }

function TMDbComboBox.getDataSource: TDataSource;
begin
  Result := Self.DataSource;
end;

function TMDbComboBox.GetField: TField;
begin
  Result := Self.DataSource.DataSet.FieldByName(Self.DataField);
end;

function TMDbComboBox.getFieldName: string;
begin
  Result := Self.DataField;
end;

function TMDbComboBox.getIsFocused: Boolean;
begin
  Result := Self.Focused;
end;

function TMDbComboBox.getReadOnly: Boolean;
begin
  Result := Self.ReadOnly;
end;

function TMDbComboBox.getText: WideString;
begin
  Result := Self.Text;
end;

procedure TMDbComboBox.setFocused;
begin
  Self.SetFocus;
end;

procedure TMDbComboBox.setReadOnly(val: Boolean);
begin
  Self.ReadOnly := val;
end;

procedure TMDbComboBox.setText(const Value: WideString);
begin
  Self.Text := Value;
end;

{ TMDBLookupComboBox }

function TMDBLookupComboBox.getDataSource: TDataSource;
begin
  Result := Self.DataSource
end;

function TMDBLookupComboBox.GetField: TField;
begin
  Result := Self.DataSource.DataSet.FieldByName(Self.DataField);
end;

function TMDBLookupComboBox.getFieldName: string;
begin
  Result := Self.DataField;
end;

function TMDBLookupComboBox.getIsFocused: Boolean;
begin
   Result := Self.Focused
end;

function TMDBLookupComboBox.getReadOnly: Boolean;
begin
  Result:= Self.ReadOnly;
end;

function TMDBLookupComboBox.getText: WideString;
begin
    Result:= Self.Text;
end;

procedure TMDBLookupComboBox.setFocused;
begin
   Self.setFocus;
end;

procedure TMDBLookupComboBox.setReadOnly(val: Boolean);
begin
  Self.ReadOnly := val;
end;

procedure TMDBLookupComboBox.setText(const Value: WideString);
begin
//var lookupDatset:=  Self.DataSource.DataSet.FieldByName(Self.DataField).LookupDataSet;
//var lookupfldname:=Self.DataSource.DataSet.FieldByName(Self.DataField).LookupResultField;
//var found:=lookupDatset.Locate(lookupfldname,Value,[]);
//if found then
//  Self.KeyValue := lookupDatset.FieldByName(Self.DataSource.DataSet.FieldByName(Self.DataField).keyfields).Value
//  else
//  raise Exception.Create('TMDBLookupComboBox setText erro text not found');
raise Exception.Create('Dont Assign Valie to TMDBLookupComboBox ');
end;

end.

