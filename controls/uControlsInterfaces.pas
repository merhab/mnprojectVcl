unit uControlsInterfaces;

interface
type
IEditable = interface
  procedure setReadOnly(val:boolean);
  procedure setText(const Value: WideString);
  function getText: WideString;
  function isFocused:Boolean;
  procedure setFocus;
  procedure editEnter(Sender: TObject);
end;
var
FocusedPrevIouslyIEditable,FocusedNowIeditable:IEditable;

implementation

end.
