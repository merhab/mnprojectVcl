program MNProject;

uses
  Vcl.Forms,
  ufrmLogin in 'forms\ufrmLogin.pas' {frmLogin},
  ufrmCon in 'frames\ufrmCon.pas' {frmCon: TDataModule},
  uconst in 'core\uconst.pas',
  utables in 'tables\utables.pas',
  ufrmLoginCtrl in 'forms\ufrmLoginCtrl.pas',
  ulocalizer in 'core\ulocalizer.pas',
  ulocalizervcl in 'core\ulocalizervcl.pas',
  uglobalLogin in 'core\uglobalLogin.pas',
  uframeSuper in 'frames\uframeSuper.pas' {FrameSuper: TFrame},
  uFrameUnite in 'frames\uFrameUnite.pas' {frameUnite: TFrame},
  udicDbServer in 'tables\udicDbServer.pas',
  udicParamLocal in 'tables\udicParamLocal.pas',
  uDicParamServer in 'tables\uDicParamServer.pas',
  uConnection in 'core\uConnection.pas',
  uDbManager in 'core\uDbManager.pas',
  uServerDbController in 'core\uServerDbController.pas',
  uTableManager in 'core\uTableManager.pas',
  uServerParamsDbController in 'core\uServerParamsDbController.pas',
  uDatabase in 'tables\uDatabase.pas',
  uframCat in 'frames\uframCat.pas' {frameCat: TFrame},
  uDbMasterSlaveCore in 'core\uDbMasterSlaveCore.pas',
  uInterfaces in 'core\uInterfaces.pas',
  ufrmBase in 'forms\ufrmBase.pas' {frmBase},
  ufrmtest in 'ufrmtest.pas' {frmTest},
  ufrmMain2 in 'forms\ufrmMain2.pas' {frmMain2},
  uFrmUnit in 'forms\uFrmUnit.pas' {frmUnit},
  ufrmCat in 'forms\ufrmCat.pas' {frmcat};

{$R *.res}

begin
  {$IFDEF DEBUG}
  ReportMemoryLeaksOnShutdown := True;
  {$ENDIF}
  Application.Initialize;

  Application.MainFormOnTaskbar := True;
    Application.CreateForm(TfrmCon, frmCon);
  Application.CreateForm(TfrmLogin, frmLogin);
  Application.CreateForm(TfrmMain2, frmMain2);
  Application.CreateForm(Tfrmcat, frmcat);
  Application.Run;
end.

